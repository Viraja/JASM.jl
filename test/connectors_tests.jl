
@variables t q(t) = 31.23 q2(t) = 1 S_S(t) = 12.5 [particulate = false] X_S(t) = 2 [particulate = true]
# Test inflow
@testset "InflowPort tests" begin
    # Test with dict input
    @named inflow = InflowPort(Dict(["X_S" => true, "S_S" => false,]))
    @test all(BioChemicalTreatment.symbolic_to_namestring.(states(inflow)) .== ["q", "S_S", "X_S"])
    @test all(Symbolics.getdefaultval.(states(inflow)) .== [0, 0, 0])
    @test all(isparticulate.(states(inflow)) .== [nothing, false, true])
    # Test with dict input and single state
    @named inflow = InflowPort(Dict(["X_S" => true,]))
    @test all(BioChemicalTreatment.symbolic_to_namestring.(states(inflow)) .== ["q", "X_S"])
    @test all(Symbolics.getdefaultval.(states(inflow)) .== [0, 0])
    @test all(isparticulate.(states(inflow)) .== [nothing, true])
    # Test with symbolic input
    @named inflow = InflowPort([X_S, S_S])
    @test all(BioChemicalTreatment.symbolic_to_namestring.(states(inflow)) .== ["q", "S_S", "X_S"])
    @test all(Symbolics.getdefaultval.(states(inflow)) .== [0, 12.5, 2])
    @test all(isparticulate.(states(inflow)) .== [nothing, false, true])
    # Test with q input
    @named inflow = InflowPort([q, X_S, S_S])
    @test all(BioChemicalTreatment.symbolic_to_namestring.(states(inflow)) .== ["q", "S_S", "X_S"])
    @test all(Symbolics.getdefaultval.(states(inflow)) .== [31.23, 12.5, 2])
    @test all(isparticulate.(states(inflow)) .== [nothing, false, true])
    # Test with q input and nothing as initial_concentrations and initial_flow
    @named inflow = InflowPort([q, X_S, S_S], initial_flow = nothing, initial_concentrations=nothing)
    @test all(BioChemicalTreatment.symbolic_to_namestring.(states(inflow)) .== ["q", "S_S", "X_S"])
    @test all(Symbolics.getdefaultval.(states(inflow)) .== [31.23, 12.5, 2])
    @test all(isparticulate.(states(inflow)) .== [nothing, false, true])
    # Test with q input at different location
    @named inflow = InflowPort([X_S, q, S_S])
    @test all(BioChemicalTreatment.symbolic_to_namestring.(states(inflow)) .== ["q", "S_S", "X_S"])
    @test all(Symbolics.getdefaultval.(states(inflow)) .== [31.23, 12.5, 2])
    @test all(isparticulate.(states(inflow)) .== [nothing, false, true])
    # Test initial flow
    @named inflow = InflowPort([X_S, S_S]; initial_flow = 35.6)
    @test all(BioChemicalTreatment.symbolic_to_namestring.(states(inflow)) .== ["q", "S_S", "X_S"])
    @test all(Symbolics.getdefaultval.(states(inflow)) .== [35.6, 12.5, 2])
    @test all(isparticulate.(states(inflow)) .== [nothing, false, true])
    # Test initial concentrations with array
    @named inflow = InflowPort([X_S, S_S]; initial_concentrations = [5, 3])
    @test all(BioChemicalTreatment.symbolic_to_namestring.(states(inflow)) .== ["q", "S_S", "X_S"])
    @test all(Symbolics.getdefaultval.(states(inflow)) .== [0, 3, 5])
    @test all(isparticulate.(states(inflow)) .== [nothing, false, true])
    # Test initial concentrations with map
    @named inflow = InflowPort([X_S, S_S]; initial_concentrations = Dict(["S_S" => 5.25, "X_S" => 123.5]))
    @test all(BioChemicalTreatment.symbolic_to_namestring.(states(inflow)) .== ["q", "S_S", "X_S"])
    @test all(Symbolics.getdefaultval.(states(inflow)) .== [0, 5.25, 123.5])
    @test all(isparticulate.(states(inflow)) .== [nothing, false, true])
    # Test if ErrorException is thrown if multiple non-components are provided
    @test_throws ErrorException InflowPort([q, q2, X_S, S_S]; initial_concentrations = [5, 3], name = :inflow)
end

# Test outflow
@testset "OutflowPort tests" begin
    # Test with dict input
    @named outflow = OutflowPort(Dict(["X_S" => true, "S_S" => false,]))
    @test all(BioChemicalTreatment.symbolic_to_namestring.(states(outflow)) .== ["q", "S_S", "X_S"])
    @test all(Symbolics.getdefaultval.(states(outflow)) .== [0, 0, 0])
    @test all(isparticulate.(states(outflow)) .== [nothing, false, true])
    # Test with dict input and single state
    @named outflow = OutflowPort(Dict(["X_S" => true,]))
    @test all(BioChemicalTreatment.symbolic_to_namestring.(states(outflow)) .== ["q", "X_S"])
    @test all(Symbolics.getdefaultval.(states(outflow)) .== [0, 0])
    @test all(isparticulate.(states(outflow)) .== [nothing, true])
    # Test with symbolic input
    @named outflow = OutflowPort([X_S, S_S])
    @test all(BioChemicalTreatment.symbolic_to_namestring.(states(outflow)) .== ["q", "S_S", "X_S"])
    @test all(Symbolics.getdefaultval.(states(outflow)) .== [0, 12.5, 2])
    @test all(isparticulate.(states(outflow)) .== [nothing, false, true])
    # Test with q input
    @named outflow = OutflowPort([q, X_S, S_S])
    @test all(BioChemicalTreatment.symbolic_to_namestring.(states(outflow)) .== ["q", "S_S", "X_S"])
    @test all(Symbolics.getdefaultval.(states(outflow)) .== [31.23, 12.5, 2])
    @test all(isparticulate.(states(outflow)) .== [nothing, false, true])
    # Test with q input and nothing for initial_flow and initial_concentrations
    @named outflow = OutflowPort([q, X_S, S_S], initial_flow = nothing, initial_concentrations = nothing)
    @test all(BioChemicalTreatment.symbolic_to_namestring.(states(outflow)) .== ["q", "S_S", "X_S"])
    @test all(Symbolics.getdefaultval.(states(outflow)) .== [31.23, 12.5, 2])
    @test all(isparticulate.(states(outflow)) .== [nothing, false, true])
    # Test with q input at different location
    @named outflow = OutflowPort([X_S, q, S_S])
    @test all(BioChemicalTreatment.symbolic_to_namestring.(states(outflow)) .== ["q", "S_S", "X_S"])
    @test all(Symbolics.getdefaultval.(states(outflow)) .== [31.23, 12.5, 2])
    @test all(isparticulate.(states(outflow)) .== [nothing, false, true])
    # Test initial flow
    @named outflow = OutflowPort([X_S, S_S]; initial_flow = 35.6)
    @test all(BioChemicalTreatment.symbolic_to_namestring.(states(outflow)) .== ["q", "S_S", "X_S"])
    @test all(Symbolics.getdefaultval.(states(outflow)) .== [35.6, 12.5, 2])
    @test all(isparticulate.(states(outflow)) .== [nothing, false, true])
    # Test initial concentrations with array
    @named outflow = OutflowPort([X_S, S_S]; initial_concentrations = [5, 3])
    @test all(BioChemicalTreatment.symbolic_to_namestring.(states(outflow)) .== ["q", "S_S", "X_S"])
    @test all(Symbolics.getdefaultval.(states(outflow)) .== [0, 3, 5])
    @test all(isparticulate.(states(outflow)) .== [nothing, false, true])
    # Test initial concentrations with map
    @named outflow = OutflowPort([X_S, S_S]; initial_concentrations = Dict(["S_S" => 5.25, "X_S" => 123.5]))
    @test all(BioChemicalTreatment.symbolic_to_namestring.(states(outflow)) .== ["q", "S_S", "X_S"])
    @test all(Symbolics.getdefaultval.(states(outflow)) .== [0, 5.25, 123.5])
    @test all(isparticulate.(states(outflow)) .== [nothing, false, true])
    # Test if ErrorException is thrown if multiple non-components are provided
    @test_throws ErrorException OutflowPort([q, q2, X_S, S_S]; initial_concentrations = [5, 3], name = :outflow)
end

# Test ReactionInputPort
@testset "ReactionInputPort tests" begin
    # Test with array input
    @named reactionInput = ReactionInputPort(["X_S", "S_S"])
    @test all(BioChemicalTreatment.symbolic_to_namestring.(states(reactionInput)) .== ["S_S", "X_S"])
    @test all(Symbolics.getdefaultval.(states(reactionInput)) .== [0, 0])
    # Test with array input and single state
    @named reactionInput = ReactionInputPort(["X_S"])
    @test all(BioChemicalTreatment.symbolic_to_namestring.(states(reactionInput)) .== ["X_S"])
    @test all(Symbolics.getdefaultval.(states(reactionInput)) .== [0])
    # Test with symbolic input
    @named reactionInput = ReactionInputPort([X_S, S_S])
    @test all(BioChemicalTreatment.symbolic_to_namestring.(states(reactionInput)) .== ["S_S", "X_S"])
    @test all(Symbolics.getdefaultval.(states(reactionInput)) .== [12.5, 2])
    # Test initial concentrations with array
    @named reactionInput = ReactionInputPort([X_S, S_S]; initial_rates = [5, 3])
    @test all(BioChemicalTreatment.symbolic_to_namestring.(states(reactionInput)) .== ["S_S", "X_S"])
    @test all(Symbolics.getdefaultval.(states(reactionInput)) .== [3, 5])
    # Test initial concentrations with map
    @named reactionInput = ReactionInputPort([X_S, S_S]; initial_rates = Dict(["S_S" => 5.25, X_S => 123.5]))
    @test all(BioChemicalTreatment.symbolic_to_namestring.(states(reactionInput)) .== ["S_S", "X_S"])
    @test all(Symbolics.getdefaultval.(states(reactionInput)) .== [5.25, 123.5])
end

# Test ReactionOutputPort
@testset "ReactionOutputPort tests" begin
    # Test with array input
    @named reactionOutput = ReactionOutputPort(["X_S", "S_S"])
    @test all(BioChemicalTreatment.symbolic_to_namestring.(states(reactionOutput)) .== ["S_S", "X_S"])
    @test all(Symbolics.getdefaultval.(states(reactionOutput)) .== [0, 0])
    # Test with array input and single state
    @named reactionOutput = ReactionOutputPort(["X_S"])
    @test all(BioChemicalTreatment.symbolic_to_namestring.(states(reactionOutput)) .== ["X_S"])
    @test all(Symbolics.getdefaultval.(states(reactionOutput)) .== [0])
    # Test with symbolic input
    @named reactionOutput = ReactionOutputPort([X_S, S_S])
    @test all(BioChemicalTreatment.symbolic_to_namestring.(states(reactionOutput)) .== ["S_S", "X_S"])
    @test all(Symbolics.getdefaultval.(states(reactionOutput)) .== [12.5, 2])
    # Test initial concentrations with array
    @named reactionOutput = ReactionOutputPort([X_S, S_S]; initial_rates = [5, 3])
    @test all(BioChemicalTreatment.symbolic_to_namestring.(states(reactionOutput)) .== ["S_S", "X_S"])
    @test all(Symbolics.getdefaultval.(states(reactionOutput)) .== [3, 5])
    # Test initial concentrations with map
    @named reactionOutput = ReactionOutputPort([X_S, S_S]; initial_rates = Dict(["S_S" => 5.25, X_S => 123.5]))
    @test all(BioChemicalTreatment.symbolic_to_namestring.(states(reactionOutput)) .== ["S_S", "X_S"])
    @test all(Symbolics.getdefaultval.(states(reactionOutput)) .== [5.25, 123.5])
end

# Test StateInputPort
@testset "StateInputPort tests" begin
    # Test with dict input
    @named stateInput = StateInputPort(Dict(["X_S" => true, "S_S" => false]))
    @test all(BioChemicalTreatment.symbolic_to_namestring.(states(stateInput)) .== ["S_S", "X_S"])
    @test all(Symbolics.getdefaultval.(states(stateInput)) .== [0, 0])
    @test all(isparticulate.(states(stateInput)) .== [false, true])
    # Test with dict input and single state
    @named stateInput = StateInputPort(Dict(["X_S" => true]))
    @test all(BioChemicalTreatment.symbolic_to_namestring.(states(stateInput)) .== ["X_S"])
    @test all(Symbolics.getdefaultval.(states(stateInput)) .== [0])
    @test all(isparticulate.(states(stateInput)) .== [true])
    # Test with symbolic input
    @named stateInput = StateInputPort([X_S, S_S])
    @test all(BioChemicalTreatment.symbolic_to_namestring.(states(stateInput)) .== ["S_S", "X_S"])
    @test all(Symbolics.getdefaultval.(states(stateInput)) .== [12.5, 2])
    @test all(isparticulate.(states(stateInput)) .== [false, true])
    # Test initial concentrations with array
    @named stateInput = StateInputPort([X_S, S_S]; initial_states = [5, 3])
    @test all(BioChemicalTreatment.symbolic_to_namestring.(states(stateInput)) .== ["S_S", "X_S"])
    @test all(Symbolics.getdefaultval.(states(stateInput)) .== [3, 5])
    @test all(isparticulate.(states(stateInput)) .== [false, true])
    # Test initial concentrations with map
    @named stateInput = StateInputPort([X_S, S_S]; initial_states = Dict(["S_S" => 5.25, X_S => 123.5]))
    @test all(BioChemicalTreatment.symbolic_to_namestring.(states(stateInput)) .== ["S_S", "X_S"])
    @test all(Symbolics.getdefaultval.(states(stateInput)) .== [5.25, 123.5])
    @test all(isparticulate.(states(stateInput)) .== [false, true])
end

# Test StateOutputPort
@testset "StateOutputPort tests" begin
    # Test with dict input
    @named stateOutput = StateOutputPort(Dict(["X_S" => true, "S_S" => false]))
    @test all(BioChemicalTreatment.symbolic_to_namestring.(states(stateOutput)) .== ["S_S", "X_S"])
    @test all(Symbolics.getdefaultval.(states(stateOutput)) .== [0, 0])
    @test all(isparticulate.(states(stateOutput)) .== [false, true])
    # Test with dict input and single state
    @named stateOutput = StateOutputPort(Dict(["X_S" => true]))
    @test all(BioChemicalTreatment.symbolic_to_namestring.(states(stateOutput)) .== ["X_S"])
    @test all(Symbolics.getdefaultval.(states(stateOutput)) .== [0])
    @test all(isparticulate.(states(stateOutput)) .== [true])
    # Test with symbolic input
    @named stateOutput = StateOutputPort([X_S, S_S])
    @test all(BioChemicalTreatment.symbolic_to_namestring.(states(stateOutput)) .== ["S_S", "X_S"])
    @test all(Symbolics.getdefaultval.(states(stateOutput)) .== [12.5, 2])
    @test all(isparticulate.(states(stateOutput)) .== [false, true])
    # Test initial concentrations with array
    @named stateOutput = StateOutputPort([X_S, S_S]; initial_states = [5, 3])
    @test all(BioChemicalTreatment.symbolic_to_namestring.(states(stateOutput)) .== ["S_S", "X_S"])
    @test all(Symbolics.getdefaultval.(states(stateOutput)) .== [3, 5])
    @test all(isparticulate.(states(stateOutput)) .== [false, true])
    # Test initial concentrations with map
    @named stateOutput = StateOutputPort([X_S, S_S]; initial_states = Dict(["S_S" => 5.25, X_S => 123.5]))
    @test all(BioChemicalTreatment.symbolic_to_namestring.(states(stateOutput)) .== ["S_S", "X_S"])
    @test all(Symbolics.getdefaultval.(states(stateOutput)) .== [5.25, 123.5])
    @test all(isparticulate.(states(stateOutput)) .== [false, true])
end


