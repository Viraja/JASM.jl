@variables t S_S(t) [particulate = false] X_S(t) [particulate = true]

@testset "isparticulate tests" begin
    @test !isparticulate(S_S)
    @test isparticulate(X_S)
    @test isnothing(isparticulate(t))
    @test isparticulate(t, default = true)
    @test !isparticulate(t, default = false)
    @test all(isparticulate.([t, S_S, X_S]) .== [nothing, false, true])
end

