@testset "Aeration tests" begin
    @named aeration = Aeration()
    @test issetequal(BioChemicalTreatment.symbolic_to_namestring.(states(aeration.state)), ["S_O"])
    @test issetequal(BioChemicalTreatment.symbolic_to_namestring.(states(aeration.rates)), ["S_O"])
    @test issetequal(string.(states(aeration)), ["state₊S_O(t)", "rates₊S_O(t)", "k_La₊u(t)"])
    @test issetequal(BioChemicalTreatment.symbolic_to_namestring.(parameters(aeration)), ["S_O_max"])
end