
@variables t q(t) = 31.23 q2(t) = 1 S_S(t) = 12.5 [particulate = false] X_S(t) = 2 [particulate = true]

@testset "ConnectorsCompatibility tests" begin
    # Test inflow
    @testset "RealInputToOutflowPort tests" begin
        # Test with dict input
        @named inflow = RealInputToOutflowPort(Dict(["X_S" => true, "S_S" => false,]))
        @test issetequal(propertynames(inflow), [:outflow, :q, :S_S, :X_S])
        @test all(BioChemicalTreatment.symbolic_to_namestring.(states(inflow.outflow)) .== ["q", "S_S", "X_S"])
        @test all(Symbolics.getdefaultval.(states(inflow.outflow)) .== [0, 0, 0])
        @test all(Symbolics.getdefaultval.(states(inflow.q)) .== [0])
        @test all(Symbolics.getdefaultval.(states(inflow.S_S)) .== [0])
        @test all(Symbolics.getdefaultval.(states(inflow.X_S)) .== [0])
        # Test with symbolic input
        @named inflow = RealInputToOutflowPort([X_S, S_S])
        @test issetequal(propertynames(inflow), [:outflow, :q, :S_S, :X_S])
        @test all(BioChemicalTreatment.symbolic_to_namestring.(states(inflow.outflow)) .== ["q", "S_S", "X_S"])
        @test all(Symbolics.getdefaultval.(states(inflow.outflow)) .== [0, 12.5, 2])
        @test all(Symbolics.getdefaultval.(states(inflow.q)) .== [0])
        @test all(Symbolics.getdefaultval.(states(inflow.S_S)) .== [12.5])
        @test all(Symbolics.getdefaultval.(states(inflow.X_S)) .== [2])
        # Test with q input
        @named inflow = RealInputToOutflowPort([q, X_S, S_S])
        @test issetequal(propertynames(inflow), [:outflow, :q, :S_S, :X_S])
        @test all(BioChemicalTreatment.symbolic_to_namestring.(states(inflow.outflow)) .== ["q", "S_S", "X_S"])
        @test all(Symbolics.getdefaultval.(states(inflow.outflow)) .== [31.23, 12.5, 2])
        @test all(Symbolics.getdefaultval.(states(inflow.q)) .== [31.23])
        @test all(Symbolics.getdefaultval.(states(inflow.S_S)) .== [12.5])
        @test all(Symbolics.getdefaultval.(states(inflow.X_S)) .== [2])
        # Test with q input at different location
        @named inflow = RealInputToOutflowPort([X_S, q, S_S])
        @test issetequal(propertynames(inflow), [:outflow, :q, :S_S, :X_S])
        @test all(BioChemicalTreatment.symbolic_to_namestring.(states(inflow.outflow)) .== ["q", "S_S", "X_S"])
        @test all(Symbolics.getdefaultval.(states(inflow.outflow)) .== [31.23, 12.5, 2])
        @test all(Symbolics.getdefaultval.(states(inflow.q)) .== [31.23])
        @test all(Symbolics.getdefaultval.(states(inflow.S_S)) .== [12.5])
        @test all(Symbolics.getdefaultval.(states(inflow.X_S)) .== [2])
        # Test initial flow
        @named inflow = RealInputToOutflowPort([X_S, S_S]; initial_flow = 35.6)
        @test issetequal(propertynames(inflow), [:outflow, :q, :S_S, :X_S])
        @test all(BioChemicalTreatment.symbolic_to_namestring.(states(inflow.outflow)) .== ["q", "S_S", "X_S"])
        @test all(Symbolics.getdefaultval.(states(inflow.outflow)) .== [35.6, 12.5, 2])
        @test all(Symbolics.getdefaultval.(states(inflow.q)) .== [35.6])
        @test all(Symbolics.getdefaultval.(states(inflow.S_S)) .== [12.5])
        @test all(Symbolics.getdefaultval.(states(inflow.X_S)) .== [2])
        # Test initial concentrations with array
        @named inflow = RealInputToOutflowPort([X_S, S_S]; initial_concentrations = [5, 3])
        @test issetequal(propertynames(inflow), [:outflow, :q, :S_S, :X_S])
        @test all(BioChemicalTreatment.symbolic_to_namestring.(states(inflow.outflow)) .== ["q", "S_S", "X_S"])
        @test all(Symbolics.getdefaultval.(states(inflow.outflow)) .== [0, 3, 5])
        @test all(Symbolics.getdefaultval.(states(inflow.q)) .== [0])
        @test all(Symbolics.getdefaultval.(states(inflow.S_S)) .== [3])
        @test all(Symbolics.getdefaultval.(states(inflow.X_S)) .== [5])
        # Test initial concentrations with map
        @named inflow = RealInputToOutflowPort([X_S, S_S]; initial_concentrations = Dict(["S_S" => 5.25, "X_S" => 123.5]))
        @test issetequal(propertynames(inflow), [:outflow, :q, :S_S, :X_S])
        @test all(BioChemicalTreatment.symbolic_to_namestring.(states(inflow.outflow)) .== ["q", "S_S", "X_S"])
        @test all(Symbolics.getdefaultval.(states(inflow.outflow)) .== [0, 5.25, 123.5])
        @test all(Symbolics.getdefaultval.(states(inflow.q)) .== [0])
        @test all(Symbolics.getdefaultval.(states(inflow.S_S)) .== [5.25])
        @test all(Symbolics.getdefaultval.(states(inflow.X_S)) .== [123.5])
    end

    # Test InflowPortToRealOutput
    @testset "InflowPortToRealOutput tests" begin
        # Test with dict input
        @named outflow = InflowPortToRealOutput(Dict(["X_S" => true, "S_S" => false,]))
        @test issetequal(propertynames(outflow), [:inflow, :q, :S_S, :X_S])
        @test all(BioChemicalTreatment.symbolic_to_namestring.(states(outflow.inflow)) .== ["q", "S_S", "X_S"])
        @test all(Symbolics.getdefaultval.(states(outflow.inflow)) .== [0, 0, 0])
        @test all(Symbolics.getdefaultval.(states(outflow.q)) .== [0])
        @test all(Symbolics.getdefaultval.(states(outflow.S_S)) .== [0])
        @test all(Symbolics.getdefaultval.(states(outflow.X_S)) .== [0])
        # Test with symbolic input
        @named outflow = InflowPortToRealOutput([X_S, S_S])
        @test issetequal(propertynames(outflow), [:inflow, :q, :S_S, :X_S])
        @test all(BioChemicalTreatment.symbolic_to_namestring.(states(outflow.inflow)) .== ["q", "S_S", "X_S"])
        @test all(Symbolics.getdefaultval.(states(outflow.inflow)) .== [0, 12.5, 2])
        @test all(Symbolics.getdefaultval.(states(outflow.q)) .== [0])
        @test all(Symbolics.getdefaultval.(states(outflow.S_S)) .== [12.5])
        @test all(Symbolics.getdefaultval.(states(outflow.X_S)) .== [2])
        # Test with q input
        @named outflow = InflowPortToRealOutput([q, X_S, S_S])
        @test issetequal(propertynames(outflow), [:inflow, :q, :S_S, :X_S])
        @test all(BioChemicalTreatment.symbolic_to_namestring.(states(outflow.inflow)) .== ["q", "S_S", "X_S"])
        @test all(Symbolics.getdefaultval.(states(outflow.inflow)) .== [31.23, 12.5, 2])
        @test all(Symbolics.getdefaultval.(states(outflow.q)) .== [31.23])
        @test all(Symbolics.getdefaultval.(states(outflow.S_S)) .== [12.5])
        @test all(Symbolics.getdefaultval.(states(outflow.X_S)) .== [2])
        # Test with q input at different location
        @named outflow = InflowPortToRealOutput([X_S, q, S_S])
        @test issetequal(propertynames(outflow), [:inflow, :q, :S_S, :X_S])
        @test all(BioChemicalTreatment.symbolic_to_namestring.(states(outflow.inflow)) .== ["q", "S_S", "X_S"])
        @test all(Symbolics.getdefaultval.(states(outflow.inflow)) .== [31.23, 12.5, 2])
        @test all(Symbolics.getdefaultval.(states(outflow.q)) .== [31.23])
        @test all(Symbolics.getdefaultval.(states(outflow.S_S)) .== [12.5])
        @test all(Symbolics.getdefaultval.(states(outflow.X_S)) .== [2])
        # Test initial flow
        @named outflow = InflowPortToRealOutput([X_S, S_S]; initial_flow = 35.6)
        @test issetequal(propertynames(outflow), [:inflow, :q, :S_S, :X_S])
        @test all(BioChemicalTreatment.symbolic_to_namestring.(states(outflow.inflow)) .== ["q", "S_S", "X_S"])
        @test all(Symbolics.getdefaultval.(states(outflow.inflow)) .== [35.6, 12.5, 2])
        @test all(Symbolics.getdefaultval.(states(outflow.q)) .== [35.6])
        @test all(Symbolics.getdefaultval.(states(outflow.S_S)) .== [12.5])
        @test all(Symbolics.getdefaultval.(states(outflow.X_S)) .== [2])
        # Test initial concentrations with array
        @named outflow = InflowPortToRealOutput([X_S, S_S]; initial_concentrations = [5, 3])
        @test issetequal(propertynames(outflow), [:inflow, :q, :S_S, :X_S])
        @test all(BioChemicalTreatment.symbolic_to_namestring.(states(outflow.inflow)) .== ["q", "S_S", "X_S"])
        @test all(Symbolics.getdefaultval.(states(outflow.inflow)) .== [0, 3, 5])
        @test all(Symbolics.getdefaultval.(states(outflow.q)) .== [0])
        @test all(Symbolics.getdefaultval.(states(outflow.S_S)) .== [3])
        @test all(Symbolics.getdefaultval.(states(outflow.X_S)) .== [5])
        # Test initial concentrations with map
        @named outflow = InflowPortToRealOutput([X_S, S_S]; initial_concentrations = Dict(["S_S" => 5.25, "X_S" => 123.5]))
        @test issetequal(propertynames(outflow), [:inflow, :q, :S_S, :X_S])
        @test all(BioChemicalTreatment.symbolic_to_namestring.(states(outflow.inflow)) .== ["q", "S_S", "X_S"])
        @test all(Symbolics.getdefaultval.(states(outflow.inflow)) .== [0, 5.25, 123.5])
        @test all(Symbolics.getdefaultval.(states(outflow.q)) .== [0])
        @test all(Symbolics.getdefaultval.(states(outflow.S_S)) .== [5.25])
        @test all(Symbolics.getdefaultval.(states(outflow.X_S)) .== [123.5])
    end

    # Test RealInputToReactionOutput
    @testset "RealInputToReactionOutput tests" begin
        # Test with array input
        @named reactionoutput = RealInputToReactionOutput(["X_S", "S_S"])
        @test issetequal(propertynames(reactionoutput), [:reactionoutput, :S_S, :X_S])
        @test all(BioChemicalTreatment.symbolic_to_namestring.(states(reactionoutput.reactionoutput)) .== ["S_S", "X_S"])
        @test all(Symbolics.getdefaultval.(states(reactionoutput.reactionoutput)) .== [0, 0])
        @test all(Symbolics.getdefaultval.(states(reactionoutput.S_S)) .== [0])
        @test all(Symbolics.getdefaultval.(states(reactionoutput.X_S)) .== [0])
        # Test with symbolic input
        @named reactionoutput = RealInputToReactionOutput([X_S, S_S])
        @test issetequal(propertynames(reactionoutput), [:reactionoutput, :S_S, :X_S])
        @test all(BioChemicalTreatment.symbolic_to_namestring.(states(reactionoutput.reactionoutput)) .== ["S_S", "X_S"])
        @test all(Symbolics.getdefaultval.(states(reactionoutput.reactionoutput)) .== [12.5, 2])
        @test all(Symbolics.getdefaultval.(states(reactionoutput.S_S)) .== [12.5])
        @test all(Symbolics.getdefaultval.(states(reactionoutput.X_S)) .== [2])
        # Test initial concentrations with array
        @named reactionoutput = RealInputToReactionOutput([X_S, S_S]; initial_rates = [5, 3])
        @test issetequal(propertynames(reactionoutput), [:reactionoutput, :S_S, :X_S])
        @test all(BioChemicalTreatment.symbolic_to_namestring.(states(reactionoutput.reactionoutput)) .== ["S_S", "X_S"])
        @test all(Symbolics.getdefaultval.(states(reactionoutput.reactionoutput)) .== [3, 5])
        @test all(Symbolics.getdefaultval.(states(reactionoutput.S_S)) .== [3])
        @test all(Symbolics.getdefaultval.(states(reactionoutput.X_S)) .== [5])
        # Test initial concentrations with map
        @named reactionoutput = RealInputToReactionOutput([X_S, S_S]; initial_rates = Dict(["S_S" => 5.25, X_S => 123.5]))
        @test issetequal(propertynames(reactionoutput), [:reactionoutput, :S_S, :X_S])
        @test all(BioChemicalTreatment.symbolic_to_namestring.(states(reactionoutput.reactionoutput)) .== ["S_S", "X_S"])
        @test all(Symbolics.getdefaultval.(states(reactionoutput.reactionoutput)) .== [5.25, 123.5])
        @test all(Symbolics.getdefaultval.(states(reactionoutput.S_S)) .== [5.25])
        @test all(Symbolics.getdefaultval.(states(reactionoutput.X_S)) .== [123.5])
    end

    # Test ReactionInputToRealOutput
    @testset "ReactionInputToRealOutput tests" begin
        # Test with array input
        @named reactioninput = ReactionInputToRealOutput(["X_S", "S_S"])
        @test issetequal(propertynames(reactioninput), [:reactioninput, :S_S, :X_S])
        @test all(BioChemicalTreatment.symbolic_to_namestring.(states(reactioninput.reactioninput)) .== ["S_S", "X_S"])
        @test all(Symbolics.getdefaultval.(states(reactioninput.reactioninput)) .== [0, 0])
        @test all(Symbolics.getdefaultval.(states(reactioninput.S_S)) .== [0])
        @test all(Symbolics.getdefaultval.(states(reactioninput.X_S)) .== [0])
        # Test with symbolic input
        @named reactioninput = ReactionInputToRealOutput([X_S, S_S])
        @test issetequal(propertynames(reactioninput), [:reactioninput, :S_S, :X_S])
        @test all(BioChemicalTreatment.symbolic_to_namestring.(states(reactioninput.reactioninput)) .== ["S_S", "X_S"])
        @test all(Symbolics.getdefaultval.(states(reactioninput.reactioninput)) .== [12.5, 2])
        @test all(Symbolics.getdefaultval.(states(reactioninput.S_S)) .== [12.5])
        @test all(Symbolics.getdefaultval.(states(reactioninput.X_S)) .== [2])
        # Test initial concentrations with array
        @named reactioninput = ReactionInputToRealOutput([X_S, S_S]; initial_rates = [5, 3])
        @test issetequal(propertynames(reactioninput), [:reactioninput, :S_S, :X_S])
        @test all(BioChemicalTreatment.symbolic_to_namestring.(states(reactioninput.reactioninput)) .== ["S_S", "X_S"])
        @test all(Symbolics.getdefaultval.(states(reactioninput.reactioninput)) .== [3, 5])
        @test all(Symbolics.getdefaultval.(states(reactioninput.S_S)) .== [3])
        @test all(Symbolics.getdefaultval.(states(reactioninput.X_S)) .== [5])
        # Test initial concentrations with map
        @named reactioninput = ReactionInputToRealOutput([X_S, S_S]; initial_rates = Dict(["S_S" => 5.25, X_S => 123.5]))
        @test issetequal(propertynames(reactioninput), [:reactioninput, :S_S, :X_S])
        @test all(BioChemicalTreatment.symbolic_to_namestring.(states(reactioninput.reactioninput)) .== ["S_S", "X_S"])
        @test all(Symbolics.getdefaultval.(states(reactioninput.reactioninput)) .== [5.25, 123.5])
        @test all(Symbolics.getdefaultval.(states(reactioninput.S_S)) .== [5.25])
        @test all(Symbolics.getdefaultval.(states(reactioninput.X_S)) .== [123.5])
    end

    # Test RealInputToStateOutput
    @testset "RealInputToStateOutput tests" begin
        # Test with dict input
        @named stateoutput = RealInputToStateOutput(Dict(["X_S" => true, "S_S" => false]))
        @test issetequal(propertynames(stateoutput), [:stateoutput, :S_S, :X_S])
        @test all(BioChemicalTreatment.symbolic_to_namestring.(states(stateoutput.stateoutput)) .== ["S_S", "X_S"])
        @test all(Symbolics.getdefaultval.(states(stateoutput.stateoutput)) .== [0, 0])
        @test all(Symbolics.getdefaultval.(states(stateoutput.S_S)) .== [0])
        @test all(Symbolics.getdefaultval.(states(stateoutput.X_S)) .== [0])
        # Test with symbolic input
        @named stateoutput = RealInputToStateOutput([X_S, S_S])
        @test issetequal(propertynames(stateoutput), [:stateoutput, :S_S, :X_S])
        @test all(BioChemicalTreatment.symbolic_to_namestring.(states(stateoutput.stateoutput)) .== ["S_S", "X_S"])
        @test all(Symbolics.getdefaultval.(states(stateoutput.stateoutput)) .== [12.5, 2])
        @test all(Symbolics.getdefaultval.(states(stateoutput.S_S)) .== [12.5])
        @test all(Symbolics.getdefaultval.(states(stateoutput.X_S)) .== [2])
        # Test initial concentrations with array
        @named stateoutput = RealInputToStateOutput([X_S, S_S]; initial_states = [5, 3])
        @test issetequal(propertynames(stateoutput), [:stateoutput, :S_S, :X_S])
        @test all(BioChemicalTreatment.symbolic_to_namestring.(states(stateoutput.stateoutput)) .== ["S_S", "X_S"])
        @test all(Symbolics.getdefaultval.(states(stateoutput.stateoutput)) .== [3, 5])
        @test all(Symbolics.getdefaultval.(states(stateoutput.S_S)) .== [3])
        @test all(Symbolics.getdefaultval.(states(stateoutput.X_S)) .== [5])
        # Test initial concentrations with map
        @named stateoutput = RealInputToStateOutput([X_S, S_S]; initial_states = Dict(["S_S" => 5.25, X_S => 123.5]))
        @test issetequal(propertynames(stateoutput), [:stateoutput, :S_S, :X_S])
        @test all(BioChemicalTreatment.symbolic_to_namestring.(states(stateoutput.stateoutput)) .== ["S_S", "X_S"])
        @test all(Symbolics.getdefaultval.(states(stateoutput.stateoutput)) .== [5.25, 123.5])
        @test all(Symbolics.getdefaultval.(states(stateoutput.S_S)) .== [5.25])
        @test all(Symbolics.getdefaultval.(states(stateoutput.X_S)) .== [123.5])
    end

    # Test StateInputToRealOutput
    @testset "StateInputToRealOutput tests" begin
        # Test with dict input
        @named stateinput = StateInputToRealOutput(Dict(["X_S" => true, "S_S" => false]))
        @test issetequal(propertynames(stateinput), [:stateinput, :S_S, :X_S])
        @test all(BioChemicalTreatment.symbolic_to_namestring.(states(stateinput.stateinput)) .== ["S_S", "X_S"])
        @test all(Symbolics.getdefaultval.(states(stateinput.stateinput)) .== [0, 0])
        @test all(Symbolics.getdefaultval.(states(stateinput.S_S)) .== [0])
        @test all(Symbolics.getdefaultval.(states(stateinput.X_S)) .== [0])
        # Test with symbolic input
        @named stateinput = StateInputToRealOutput([X_S, S_S])
        @test issetequal(propertynames(stateinput), [:stateinput, :S_S, :X_S])
        @test all(BioChemicalTreatment.symbolic_to_namestring.(states(stateinput.stateinput)) .== ["S_S", "X_S"])
        @test all(Symbolics.getdefaultval.(states(stateinput.stateinput)) .== [12.5, 2])
        @test all(Symbolics.getdefaultval.(states(stateinput.S_S)) .== [12.5])
        @test all(Symbolics.getdefaultval.(states(stateinput.X_S)) .== [2])
        # Test initial concentrations with array
        @named stateinput = StateInputToRealOutput([X_S, S_S]; initial_states = [5, 3])
        @test issetequal(propertynames(stateinput), [:stateinput, :S_S, :X_S])
        @test all(BioChemicalTreatment.symbolic_to_namestring.(states(stateinput.stateinput)) .== ["S_S", "X_S"])
        @test all(Symbolics.getdefaultval.(states(stateinput.stateinput)) .== [3, 5])
        @test all(Symbolics.getdefaultval.(states(stateinput.S_S)) .== [3])
        @test all(Symbolics.getdefaultval.(states(stateinput.X_S)) .== [5])
        # Test initial concentrations with map
        @named stateinput = StateInputToRealOutput([X_S, S_S]; initial_states = Dict(["S_S" => 5.25, X_S => 123.5]))
        @test issetequal(propertynames(stateinput), [:stateinput, :S_S, :X_S])
        @test all(BioChemicalTreatment.symbolic_to_namestring.(states(stateinput.stateinput)) .== ["S_S", "X_S"])
        @test all(Symbolics.getdefaultval.(states(stateinput.stateinput)) .== [5.25, 123.5])
        @test all(Symbolics.getdefaultval.(states(stateinput.S_S)) .== [5.25])
        @test all(Symbolics.getdefaultval.(states(stateinput.X_S)) .== [123.5])
    end
end

