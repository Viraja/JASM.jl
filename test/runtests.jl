using BioChemicalTreatment
using ModelingToolkit
using ModelingToolkitStandardLibrary
using Test

@testset "BioChemicalTreatment.jl" begin
    
    @testset "ASM1 tests" begin
		include("asm1_test.jl")
	end

    @testset "GrowthModel tests" begin
		include("growthmodel_tests.jl")
	end
    
    @testset "Variable tests" begin
		include("variables_tests.jl")
	end
    
    @testset "Utils tests" begin
		include("utils_tests.jl")
	end
    
    @testset "Connector tests" begin
		include("connectors_tests.jl")
	end
    
    @testset "Reactor tests" begin
		include("reactors_test.jl")
	end
    
    @testset "ModelingToolkitStandardLibrary compatibility tests" begin
		include("modelingtoolkitstandardlibrarycompat_tests.jl")
	end

	@testset "Flowelements tests" begin
		include("flowelements_tests.jl")
	end

end
