@variables t S_S(t) [particulate = false] X_S(t) [particulate = true]

@testset "CSTR tests" begin
    @named cstr = CSTR(1500, [S_S, X_S], initial_states = (S_S => 1, X_S => 2))
    @test issetequal(BioChemicalTreatment.symbolic_to_namestring.(states(cstr.inflow)), ["q", "S_S", "X_S"])
    @test issetequal(BioChemicalTreatment.symbolic_to_namestring.(states(cstr.outflow)), ["q", "S_S", "X_S"])
    @test issetequal(BioChemicalTreatment.symbolic_to_namestring.(states(cstr.rates)), ["S_S", "X_S"])
    @test issetequal(BioChemicalTreatment.symbolic_to_namestring.(states(cstr.state)), ["S_S", "X_S"])
    @test issetequal(
        string.(states(cstr)), 
        [
            "S_S(t)", "X_S(t)",
            "inflow₊q(t)", "inflow₊S_S(t)", "inflow₊X_S(t)",
            "outflow₊q(t)", "outflow₊S_S(t)", "outflow₊X_S(t)",
            "state₊S_S(t)", "state₊X_S(t)",
            "rates₊S_S(t)", "rates₊X_S(t)",
        ]
    )
    @test all(isparticulate.([S_S, X_S]) .== [false, true])
    @test all(Symbolics.getdefaultval.([cstr.S_S, cstr.X_S]) .== [1, 2])
end

