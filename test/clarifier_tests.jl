@variables t S_S(t) [particulate = false] X_S(t) [particulate = true]

@testset "IdealClarifier tests" begin
    @named clarifier = IdealClarifier([S_S, X_S]; f_ns = 0)
    @test issetequal(BioChemicalTreatment.symbolic_to_namestring.(states(clarifier.inflow)), ["q", "S_S", "X_S"])
    @test issetequal(BioChemicalTreatment.symbolic_to_namestring.(states(clarifier.sludge)), ["q", "S_S", "X_S"])
    @test issetequal(BioChemicalTreatment.symbolic_to_namestring.(states(clarifier.effluent)), ["q", "S_S", "X_S"])
    @test issetequal(
        string.(states(clarifier)), 
        [
            "inflow₊q(t)", "inflow₊S_S(t)", "inflow₊X_S(t)",
            "sludge₊q(t)", "sludge₊S_S(t)", "sludge₊X_S(t)",
            "effluent₊q(t)", "effluent₊S_S(t)", "effluent₊X_S(t)",
        ]
    )
    @test issetequal(BioChemicalTreatment.symbolic_to_namestring.(parameters(clarifier)), ["f_ns"])
    @test length(equations(clarifier)) == (1 + 2 + 2) # MassConservation Flow + 2*MassConservation Components + 2*Components in sludge
end
