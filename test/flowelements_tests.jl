@variables t S_S(t) [particulate = false] X_S(t) [particulate = true]

@testset "FlowConnector tests" begin
    @named connector = FlowConnector([S_S, X_S]; n_in = 2, n_out = 2)
    @test issetequal(BioChemicalTreatment.symbolic_to_namestring.(states(connector.inflow_1)), ["q", "S_S", "X_S"])
    @test issetequal(BioChemicalTreatment.symbolic_to_namestring.(states(connector.inflow_2)), ["q", "S_S", "X_S"])
    @test issetequal(BioChemicalTreatment.symbolic_to_namestring.(states(connector.outflow_1)), ["q", "S_S", "X_S"])
    @test issetequal(BioChemicalTreatment.symbolic_to_namestring.(states(connector.outflow_2)), ["q", "S_S", "X_S"])
    @test issetequal(
        string.(states(connector)), 
        [
            "inflow_1₊q(t)", "inflow_1₊S_S(t)", "inflow_1₊X_S(t)",
            "inflow_2₊q(t)", "inflow_2₊S_S(t)", "inflow_2₊X_S(t)",
            "outflow_1₊q(t)", "outflow_1₊S_S(t)", "outflow_1₊X_S(t)",
            "outflow_2₊q(t)", "outflow_2₊S_S(t)", "outflow_2₊X_S(t)",
        ]
    )
    @test length(equations(connector)) == (1 + 2 + 2) # MassConservation Flow + 2*MassConservation Components + 2*Outflow Components equal
end

@testset "FlowUnifier tests" begin
    @named unifier = FlowUnifier([S_S, X_S]; n_in = 2)
    @test issetequal(BioChemicalTreatment.symbolic_to_namestring.(states(unifier.inflow_1)), ["q", "S_S", "X_S"])
    @test issetequal(BioChemicalTreatment.symbolic_to_namestring.(states(unifier.inflow_2)), ["q", "S_S", "X_S"])
    @test issetequal(BioChemicalTreatment.symbolic_to_namestring.(states(unifier.outflow)), ["q", "S_S", "X_S"])
    @test issetequal(
        string.(states(unifier)), 
        [
            "inflow_1₊q(t)", "inflow_1₊S_S(t)", "inflow_1₊X_S(t)",
            "inflow_2₊q(t)", "inflow_2₊S_S(t)", "inflow_2₊X_S(t)",
            "outflow₊q(t)", "outflow₊S_S(t)", "outflow₊X_S(t)"
        ]
    )
    @test length(equations(unifier)) == (1 + 2) # MassConservation Flow + 2*MassConservation Components
end

@testset "FlowSeparator tests" begin
    @named separator = FlowSeparator([S_S, X_S]; n_out = 2)
    @test issetequal(BioChemicalTreatment.symbolic_to_namestring.(states(separator.inflow)), ["q", "S_S", "X_S"])
    @test issetequal(BioChemicalTreatment.symbolic_to_namestring.(states(separator.outflow_1)), ["q", "S_S", "X_S"])
    @test issetequal(BioChemicalTreatment.symbolic_to_namestring.(states(separator.outflow_2)), ["q", "S_S", "X_S"])
    @test issetequal(
        string.(states(separator)), 
        [
            "inflow₊q(t)", "inflow₊S_S(t)", "inflow₊X_S(t)",
            "outflow_1₊q(t)", "outflow_1₊S_S(t)", "outflow_1₊X_S(t)",
            "outflow_2₊q(t)", "outflow_2₊S_S(t)", "outflow_2₊X_S(t)",
        ]
    )
    @test length(equations(separator)) == (1 + 2 + 2) # MassConservation Flow + 2*MassConservation Components + 2*Outflow Components equal
end

@testset "FlowPump tests" begin
    # Test with given flowrate
    @named pump = FlowPump([S_S, X_S]; flowrate=1)
    @test issetequal(BioChemicalTreatment.symbolic_to_namestring.(states(pump.inflow)), ["q", "S_S", "X_S"])
    @test issetequal(BioChemicalTreatment.symbolic_to_namestring.(states(pump.outflow)), ["q", "S_S", "X_S"])
    @test issetequal(
        string.(states(pump)), 
        [
            "inflow₊q(t)", "inflow₊S_S(t)", "inflow₊X_S(t)",
            "outflow₊q(t)", "outflow₊S_S(t)", "outflow₊X_S(t)",
        ]
    )
    @test issetequal(BioChemicalTreatment.symbolic_to_namestring.(parameters(pump)), ["q_pumped"])
    @test length(equations(pump)) == (1 + 1) # Connect inflow and outflow + Pumprate
    # Test with flowrate port
    @named pump = FlowPump([S_S, X_S])
    @test issetequal(BioChemicalTreatment.symbolic_to_namestring.(states(pump.inflow)), ["q", "S_S", "X_S"])
    @test issetequal(BioChemicalTreatment.symbolic_to_namestring.(states(pump.outflow)), ["q", "S_S", "X_S"])
    @test issetequal(
        string.(states(pump)), 
        [
            "inflow₊q(t)", "inflow₊S_S(t)", "inflow₊X_S(t)",
            "outflow₊q(t)", "outflow₊S_S(t)", "outflow₊X_S(t)",
            "q_pumped₊u(t)"
        ]
    )
    @test issetequal(BioChemicalTreatment.symbolic_to_namestring.(parameters(pump)), [])
    @test length(equations(pump)) == (1 + 1) # Connect inflow and outflow + Pumprate
end
