
@variables t S_S(t) X_S(t) S_O(t)
@named sys1 = ODESystem(Equation[], t, [S_S, S_O], [])
@named sys2 = ODESystem(Equation[], t, [X_S], [])
@named composed_sys = compose(sys2, sys1)

# Test symbolic_to_namestring
@testset "symbolic_to_namestring tests" begin
    @test issetequal(BioChemicalTreatment.symbolic_to_namestring.([t, X_S, S_S, S_O]), ["t", "X_S", "S_S", "S_O"])
    @test issetequal(BioChemicalTreatment.symbolic_to_namestring.(states(composed_sys)), ["X_S", "S_S", "S_O"])
end

# Test ic_to_array
@testset "ic_to_array tests" begin
    @test all(BioChemicalTreatment.ic_to_array(["S_S", "X_S", "S_O"], [1, 2, 3.8]) .== [1, 2, 3.8])
    @test all(BioChemicalTreatment.ic_to_array(["S_S", "X_S", "S_O"], Dict([X_S => 2, S_S => 1, S_O => 3.8])) .== [1, 2, 3.8])
    @test all(BioChemicalTreatment.ic_to_array(["S_S", "X_S", "S_O"], Dict([:X_S => 2, :S_S => 1, :S_O => 3.8])) .== [1, 2, 3.8])
    @test all(BioChemicalTreatment.ic_to_array(["S_S", "X_S", "S_O"], Dict(["X_S" => 2, "S_S" => 1, "S_O" => 3.8])) .== [1, 2, 3.8])
    @test all(BioChemicalTreatment.ic_to_array(["S_S", "X_S", "S_O"], Dict([Symbolics.unwrap(X_S) => 2, Symbolics.unwrap(S_S) => 1, Symbolics.unwrap(S_O) => 3.8])) .== [1, 2, 3.8])
    @test all(BioChemicalTreatment.ic_to_array(["S_S", "X_S", "S_O"], Dict(["X_S" => 2, S_S => 1, :S_O => 3.8])) .== [1, 2, 3.8])
    @test all(BioChemicalTreatment.ic_to_array(["S_S", "X_S", "S_O"], Dict([X_S => 2, S_S => 1])) .== [1, 2, 0])
    @test all(BioChemicalTreatment.ic_to_array(["S_S", "X_S", "S_O"], Dict([S_O => 3.8, S_S => 1])) .== [1, 0, 3.8])
    @test all(BioChemicalTreatment.ic_to_array(["S_S", "X_S", "S_O"], Dict([X_S => 2, S_O => 3.8])) .== [0, 2, 3.8])
    @test all(BioChemicalTreatment.ic_to_array(["S_S", "X_S", "S_O"], [(X_S => 2), (S_S => 1), (S_O => 3.8)]) .== [1, 2, 3.8])
    @test all(BioChemicalTreatment.ic_to_array(["S_S", "X_S", "S_O"], (X_S => 2, S_S => 1, S_O => 3.8)) .== [1, 2, 3.8])
    @test all(BioChemicalTreatment.ic_to_array(["S_S", "X_S", "S_O"], []) .== [0, 0, 0])
end

