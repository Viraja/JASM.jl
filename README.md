# New repository
We moved the repository. Please check out the latest development here: https://gitlab.com/datinfo/BioChemicalTreatment.jl

# BioChemicalTreatment.jl

The latest documentation, including how to install the package, is hosted online at https://viraja.gitlab.io/BioChemicalTreatment.jl/dev/

## Development

The package was created running the following command:

```julia
using PkgTemplates
t = Template( ;
  user="viraja",
  host="gitlab.com",
  authors="Florian Wenk, Juan Pablo Carbajal, Andreas Froemelt, Mariane Yvonne Schneider",
  julia=v"1.9",
  plugins=[
    License(; name="GPL-3.0+"),
    GitLabCI(),
    Documenter{GitLabCI}(),
    ],
  )
generate("BioChemicalTreatment", t)
```
