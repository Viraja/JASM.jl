using BioChemicalTreatment
using Documenter

DocMeta.setdocmeta!(BioChemicalTreatment, :DocTestSetup, :(using BioChemicalTreatment); recursive=true)

makedocs(;
    modules=[BioChemicalTreatment],
    authors="Florian Wenk and Juan Pablo Carbajal and Andreas Froemelt and Mariane Schneider",
    repo="https://gitlab.com/viraja/BioChemicalTreatment.jl/blob/{commit}{path}#{line}",
    sitename="BioChemicalTreatment.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://viraja.gitlab.io/BioChemicalTreatment.jl",
        edit_link="main",
        assets=String[],
        repolink="https://gitlab.com/viraja/BioChemicalTreatment.jl/blob/{commit}{path}#{line}",
        #size_threshold_ignore= ["examples/ASM1_example.md"],  # see https://github.com/JuliaDocs/Documenter.jl/blob/master/CHANGELOG.md#added-1
        #example_size_threshold = 10000000
    ),
    doctest = true,
    pages=[
        "Home" => "index.md",
        "API" => "api.md",
        "Examples" => "examples/examples.md",
    ],
    warnonly = true,  # warn only about failing docs https://github.com/JuliaDocs/Documenter.jl/blob/master/CHANGELOG.md#breaking
)
