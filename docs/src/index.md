# BioChemicalTreatment.jl

*A tool for modelling biochemical treatment processes such as wastewater treatment in Julia.*

This package currently contains the activated sludge model (ASM) number one as a library, however more processes will be added and also factories to set up own models will be created.

## Repository
The code for the module is hosted at [BioChemicalTreatment](https://gitlab.com/viraja/BioChemicalTreatment.jl).

## Feature summary
The idea behind BioChemicalTreatment.jl is to create a package that can be used to model any biochemical treatment process. Different reactor types will be included as well as different biochemical models.

### Feature List
- reactors (currently only CSTR is included)
- biochemical models (currently ASM and a ozone disinfection process are included.)

## Install and remove
To install the package and test it run:

```julia
julia --project=.
using Pkg
] # to open the package manager prompt
add https://gitlab.com/Viraja/BioChemicalTreatment.jl
# go back with backspace
BioChemicalTreatment.welcome() # returns "welcome to BioChemicalTreatment"
```

You can remove the package with

```julia
]
rm BioChemicalTreatment
```

## User Manual

- [Examples](@ref)

## Reference Manual
```@contents
Pages = ["api.md"]
```

## Contributing
The prefered way of contributing is if you raise an issue using the [issue tracker](https://gitlab.com/Viraja/BioChemicalTreatment.jl/-/issues).

## Citation

If you use ModelingToolkit in your work, please cite the following, but note that the order of the authors can arbitrarily be chosen:

```
@misc{2023biochemicaltreatment,
	title = {{BioChemicalTreatment.jl} to model biochemical processes in water and wastewater treatment},
	url = {https://gitlab.com/Viraja/BioChemicalTreatment.jl},
	author = {Florian Wenk and Juan Pablo Carbajal and Andreas Froemelt and Mariane Yvonne Schneider},
	year = {2023}
}
```

## Development

### Documentation
To build the documentation locally you need to instantiate the `docs` project.
To do this run the following command from the root folder of the repository:

```
julia --project=docs -e 'using Pkg; Pkg.develop(PackageSpec(path=pwd())); Pkg.instantiate();'
```
!!! warning
    in some consoles you might need to replace `'` with `"`

You need to do this only once, or when you change the dependencies for the documentation.
Once done you can generate the documentation with the command:

```
 julia --project=docs -e 'using BioChemicalTreatment; include("docs/make.jl")'
```

!!! warning
    in some consoles you might need to replace `'` with `"` and use `\"` for the internal quotes.

The generated HTML will be in the `docs/build` folder.

To run the doctests you can run:

```
julia --project=docs -e 'using Documenter: doctest; doctest(BioChemicalTreatment);'
```

This command can be joined with the previous one to run doctests and build the docs.







