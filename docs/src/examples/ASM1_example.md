# Activated Sludge Model (ASM1)

In this example you see how you can set up the rates of an ASM1.

### ASM1 rates

This example uses the following dependencies:

``` @example asm
using BioChemicalTreatment
using ModelingToolkit
using Plots
```

If you do not have them already in your system you need to add them using `Pkg`.

The rates for the ASM1 model are defined in the function [`ASM1`](@ref).

``` @example asm
@named asm = ASM1()
asm #hide
nothing  #hide
```

As seen above, the function builds the model with predefined values for the parameters.
The set of system of equations (the rates) can accessed using

``` @example asm
equations(asm)
```

### ASM1 as system of ODEs
As you can see above, [`ASM1`](@ref) only returns a model with the rates and the states.
This is useful to connect the ASM1 model to other models in the library.
However, if you want the ASM1 as an ODE system, you can convert the rates above by doing:

``` @example asm
@named asm_ode = BioChemicalTreatment.rates_to_ode(asm)
```

You can run simulations with the result.
For that we need initial conditions.

!!! note
    
	TODO: Inital conditions hepler for asm1


``` @example asm
t = ModelingToolkit.get_iv(asm_ode)   # get independent variable
vars = @variables X_BH(t) S_NH(t) S_S(t) X_ND(t) X_S(t) S_ND(t) X_BA(t) S_ALK(t) X_P(t) S_NO(t)
st0 = Dict( X_BH  => 221.0,
            S_NH  => 50.0,
            S_S   => 13.0,
            X_ND  => 0.6,
            X_S   => 8.8,
            S_ND  => 0.6,
            X_BA  => 289.0,
            S_ALK => 582.6,
            X_P   => 72.0,
            S_NO  => 27.4
            )
st0 = [v => haskey(st0, v) ? st0[v] : 0.0 for v in states(asm_ode)] # this adds S_I, X_I and S_O => 0.0
```

Which is used to discretize the symblic ODE:

``` @example asm
asm_ode = structural_simplify(asm_ode)
prob = ODEProblem(asm_ode, st0, jac=true)
```

To get a solution we use an ODE solver:

``` @example asm
using DifferentialEquations
sol = ModelingToolkit.solve(prob, Tsit5(), tspan=(0, 1))
plot(sol)
```

### User defined parameters
If you want to provide your own parameters, you can do it the following way:

``` @example asm
params = (
	Y_A = 0.24, 	# autotrophic yield (gCOD/gCOD)
	Y_H = 0.67, 	# heterotrophic yield (gCOD/gCOD)
	f_P = 0.08, 	# fraction of biomass leading to particulate material (-)
	i_XB = 0.086, 	# nitrogen fraction in biomass gN/gCOD
	i_XP = 0.01, 	# nitrogen fraction in endogenous mass (gN/gCOD)
	μ_H = 6.0, 	# maximum specific growth rate of heterotrophs (1/day)
	K_S = 20.0, 	# substrate saturation constant (gCOD/m3)
	K_OH = 0.2, 	# Oxygen saturation constant (gO2/m3)
	K_NO = 0.5, 	# nitrate saturation constant (gNO3-N/m3)
	b_H = 0.62, 	# specific decay rate of heterotrophs (1/day)
	η_g = 0.8, 	# anoxic growth correction factor for heterotrophs (-)
	μ_A = 0.8, 	# maximum specific growth rate of autotrophs (1/day)
	K_NH = 1.0, 	# ammonium saturation constant (gO2/m3)
	K_OA = 0.4, 	# oxygen saturation constant for autotrophs (gO2/m3)
	b_A = 0.1, 	# specific decay rate of autotrophs (1/day)
	κ_h = 3.0, 	# maximum specific hydrolysis rate (1/day)
	η_h = 0.4, 	# anoxic hydrolysis correction factor
	K_X = 0.03, 	# half-saturation coefficient for hydrolysis of XS
	κ_a = 0.08 	# ammonification rate constant
);
@named asm = ASM1(;params...)
nothing  #hide
```

There we used splattering to let the function know that there is an arbitrary number of keywords.
Exactly the same example also works if you are using a `ComponentVector` from `ComponentArrays.jl` with the small difference that `params = ComponentVector()`. Analogously it works with a Dictionary. Please see [hybrid BSM1 example](@ref hybrid)`

You can also pass only some of the parameters or specify them by hand, e.g.:

```@example asm
@named asm = ASM1(;b_A=0.1)  # replace the default value for the specific decay rate of autotrophs (1/day)
nothing  #hide
```
