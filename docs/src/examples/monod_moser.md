# Monod and Moser growth

General equation

```@example monodmoser
using BioChemicalTreatment
using ModelingToolkit
@variables t S(t) B(t)
@parameters μ Kₛ n
@named mm = monod_moser(B, S; μmax = μ, K = Kₛ, n = n)
```

By default $n = 1$, the Monod model

```@example monodmoser
@named monod = monod_moser(B, S; μmax = μ, K = Kₛ)
```

Setting $n \neq 1$ gives the Moser model

```@example monodmoser
@named moser = monod_moser(B, S; μmax = μ, K = Kₛ, n = 2)
```
