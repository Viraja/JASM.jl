# [Hybrid BSM1 Example](@id hybrid)

This file presents an example of a hybrid model representing a system similar to the [BSM1](http://iwa-mia.org/wp-content/uploads/2019/04/BSM_TG_Tech_Report_no_1_BSM1_General_Description.pdf). The goal is to integrate tightly with the [ModelingToolkit.jl](https://docs.sciml.ai/ModelingToolkit/stable/) library such that the functionality from this ecosystem can be used.
First, the low-level API is shown and then further down the intended High-Level API.

Note that this is still work-in-progress and will currently not run, but it presents the targeted API and should be executable once the development has proceeded. During the development, this API will probably be adapted to match the exact syntax.

## Low-Level API
This section shows the targeted low-level API, additional functions providing a more convenient high-level API drafted below will be added, once the low-level API is running.

### Setup
First, some packages have to be imported:

``` @example lowlevel
using BioChemicalTreatment
using DifferentialEquations
using ModelingToolkit
using ModelingToolkitStandardLibrary.Blocks: Constant, Add
using Plots
#using ForwardDiff
#using Optimization, OptimizationOptimisers
#using Lux, Random
#using CSV, DataFrames
```

The next step is to load data and define the needed parameters

```@example lowlevel
## Define the file with the reference output for the training of the hybrid model
output_data_filename = "output_data.csv"

## Parameters
# ASM Parameters
params = Dict(
	:Y_A => 0.24, # autotrophic yield
	:Y_H => 0.67, # heterotrophic yield
	:f_P => 0.08, # fraction of biomass yielding particulate procucts
	:i_XB => 0.08, # mass N / mass COD in biomass
	:i_XP => 0.06, # mass N / mass COD in products from biomass
	:μ_H => 4.0, # Heterotrophic growth and decay
	:K_S => 10.0,
	:K_OH => 0.2,
	:K_NO => 0.5,
	:b_H => 0.3,
	:η_g => 0.8, # correction factor for anoxic growth of heterotrophs
	:η_h => 0.8, # correction factor for anoxic hydrolysis
	:κ_h => 3.0, # hydrolysis
	:K_X => 0.1,
	:μ_A => 0.5, # Autotrophic growth and decay
	:K_NH => 1.0,
	:b_A => 0.05,
	:K_OA => 0.4,
	:κ_a => 0.05, # ammonification
)
params_aeration = Dict([
	:k_La => 240, # oxygen transfer coefficient [d^-1]
	:S_O_max => 8 # Oxygen saturation concentration
])

# Volumes
V_na = 1000 # Volume [m^3]
V_a = 1333 # Volume [m^3]

# Initial conditions
c_init = Dict(
	:S_I => 1.0,
	:S_S => 1.0,
	:X_I => 1.0,
	:X_S => 1.0,
	:X_BH => 1.0,
	:X_BA => 1.0,
	:X_P => 1.0,
	:S_O => 1.0,
	:S_NO => 1.0,
	:S_NH => 1.0,
	:S_ND => 1.0,
	:X_ND => 1.0,
	:S_ALK => 1.0
)
nothing #hide
```

### System Creation
Once this is done, we start by creating the individual systems, which later will be connected to form the overall BSM1 model.

```@example lowlevel
## Build up model parts
# Time variable
@variables t

# Inflow, one for each variable (should not be neccessary anymore with the high-level interface)
@named inflow_q = Constant(k = 18446) # Flow [m^3/d]
@named inflow_S_I = Constant(k = 30.00) # [g COD / m^3]
@named inflow_S_S = Constant(k = 69.50) # [g COD / m^3]
@named inflow_X_I = Constant(k = 51.20) # [g COD / m^3]
@named inflow_X_S = Constant(k = 202.32) # [g COD / m^3]
@named inflow_X_BH = Constant(k = 28.17) # [g COD / m^3]
@named inflow_X_BA = Constant(k = 0.0) # [g COD / m^3]
@named inflow_X_P = Constant(k = 0.0) # [g COD / m^3]
@named inflow_S_O = Constant(k = 0.0) # [g COD / m^3]
@named inflow_S_NO = Constant(k = 0.0) # [g N / m^3]
@named inflow_S_NH = Constant(k = 31.56) # [g N / m^3]
@named inflow_S_ND = Constant(k = 6.95) # [g N / m^3]
@named inflow_X_ND = Constant(k = 10.59) # [g N / m^3]
@named inflow_S_ALK = Constant(k = 7.0) # [mol / m^3]

# Flow elements
# The ASM as the model to have the sates of it.
@named asm = ASM1()
@named inflow = RealInputToOutflowPort(states(asm.state))
@named clarifier = IdealClarifier(states(asm.state)) # Determine which are solid etc over variable annotations
@named clarifier_pump = FlowPump(states(asm.state); flowrate=18446.0 + 385.0) # Flow Rate from BSM1 Paper
@named unifier = FlowUnifier(states(asm.state); n_in=2)
@named wastage_separator = FlowSeparator(states(asm.state); n_out=2)
@named wastage_pump = FlowPump(states(asm.state); flowrate = 385)
nothing #hide
```

The Reactors are built up from several parts: The CSTR block itself provides only the mass transfer equations and has input ports for the input concentrations as well as the reaction rates. This enables the combination of different processes (e.g. standard ASM1 and added aeration).

The first two reactors are non-aerated, thus the only added process is ASM1. The connection of the ASM1 rates to the rate ports of the CSTR is already done here.

```@example lowlevel
# Define the reactors, first two are non-aerated
# First Reactor, non-aerated CSTR with ASM1
@named asm_r1 = ASM1(;params...)
@named reactor1 = CSTR(V_na, states(asm_r1.state), initial_states = c_init)
reactor1_eqs = [  
	connect(asm_r1.rates, reactor1.rates), # Rates from asm to reactor
	connect(reactor1.state, asm_r1.state), # State from reactor to asm to compute the rates
]

# Second Reactor, non-aerated CSTR with ASM1
@named asm_r2 = ASM1(;params...)
@named reactor2 = CSTR(V_na, states(asm_r2.state), initial_states = c_init)
reactor2_eqs = [  
	connect(asm_r2.rates, reactor2.rates), # Rates from asm to reactor
	connect(reactor2.state, asm_r2.state), # State from reactor to asm to compute the rates
]
nothing #hide
```

The third and fourth reactors are aerated. Thus the aeration process is added to the ASM1 reaction rate of `S_O` before being connected to the CSTR rate port.

```@example lowlevel
# Third Reactor, aerated CSTR with ASM1
@named asm_r3 = ASM1(;params...)
@named reactor3 = CSTR(V_na, states(asm_r3.state), initial_states = c_init)
@named aeration3 = Aeration(;params_aeration...)
@named add3 = Add()
reactor3_eqs = [
	# Rates from asm to reactor (except S_O), cannot use connect() directly because not all rates are connected this way
	asm_r3.rates.S_I ~ reactor3.rates.S_I,
	asm_r3.rates.S_S ~ reactor3.rates.S_S,
	asm_r3.rates.X_I ~ reactor3.rates.X_I,
	asm_r3.rates.X_S ~ reactor3.rates.X_S,
	asm_r3.rates.X_BH ~ reactor3.rates.X_BH,
	asm_r3.rates.X_BA ~ reactor3.rates.X_BA,
	asm_r3.rates.X_P ~ reactor3.rates.X_P,
	asm_r3.rates.S_NO ~ reactor3.rates.S_NO,
	asm_r3.rates.S_NH ~ reactor3.rates.S_NH,
	asm_r3.rates.S_ND ~ reactor3.rates.S_ND,
	asm_r3.rates.X_ND ~ reactor3.rates.X_ND,
	asm_r3.rates.S_ALK ~ reactor3.rates.S_ALK,
	# Rate for S_O (asm + aeration)
	add3.input1.u ~ asm_r3.rates.S_O,
	add3.input2.u ~ aeration3.rates.S_O,
	add3.output.u ~ reactor3.rates.S_O,
	# States to asm and aeration
	connect(reactor3.state, asm_r3.state),
	reactor3.state.S_O ~ aeration3.state.S_O,
	# Aeration Oxygen Transfer coefficient
	aeration3.k_La.u ~ params_aeration[:k_La]
]

# Fourth Reactor, aerated CSTR with ASM1
@named asm_r4 = ASM1(;params...)
@named reactor4 = CSTR(V_na, states(asm_r4.state),initial_states = c_init)
@named aeration4 = Aeration(;params_aeration...)
@named add4 = Add()
reactor4_eqs = [
	# Rates from asm to reactor (except S_O), cannot use connect() directly because not all rates are connected this way
	asm_r4.rates.S_I ~ reactor4.rates.S_I,
	asm_r4.rates.S_S ~ reactor4.rates.S_S,
	asm_r4.rates.X_I ~ reactor4.rates.X_I,
	asm_r4.rates.X_S ~ reactor4.rates.X_S,
	asm_r4.rates.X_BH ~ reactor4.rates.X_BH,
	asm_r4.rates.X_BA ~ reactor4.rates.X_BA,
	asm_r4.rates.X_P ~ reactor4.rates.X_P,
	asm_r4.rates.S_NO ~ reactor4.rates.S_NO,
	asm_r4.rates.S_NH ~ reactor4.rates.S_NH,
	asm_r4.rates.S_ND ~ reactor4.rates.S_ND,
	asm_r4.rates.X_ND ~ reactor4.rates.X_ND,
	asm_r4.rates.S_ALK ~ reactor4.rates.S_ALK,
	# Rate for S_O (asm + aeration)
	add4.input1.u ~ asm_r4.rates.S_O,
	add4.input2.u ~ aeration4.rates.S_O,
	add4.output.u ~ reactor4.rates.S_O,
	# States to asm and aeration
	connect(reactor4.state, asm_r4.state),
	reactor4.state.S_O ~ aeration4.state.S_O,
	# Aeration Oxygen Transfer coefficient
	aeration4.k_La.u ~ params_aeration[:k_La]
]
nothing #hide
```

Finally, the fifth reactor is aerated and contains the hybrid process. The neural net in the hybrid process is fully defined by the user to give full control over it. This process is then added similarly to the aeration process.

```@example lowlevel
# Fifth Reactor, aerated CSTR with ASM1 and hybrid process
@named asm_r5 = ASM1(;params...)
@named reactor5 = CSTR(V_na, states(asm_r5.state),initial_states = c_init)
@named aeration5 = Aeration(;params_aeration...)
@named add5 = Add()
#@named add5_hyb = Add()
# Hybrid process, (S_O, S_NH) -> S_NH
#rng = Random.default_rng()
#NN = Lux.Chain(Lux.Dense(2, 1, tanh))
#p_nn_init = Lux.setup(rng, NN)
#p_nn = ComponentVector(nn_weights = p_NN_init)
#@named hybrid_process = HybridProcess(NN, variables_in = [:S_O, :S_NH], variables_out = [:S_NH])

reactor5_eqs = [
	# Rates from asm to reactor (except S_O and S_NH), cannot use connect() directly because not all rates are connected this way
	asm_r5.rates.S_I ~ reactor5.rates.S_I,
	asm_r5.rates.S_S ~ reactor5.rates.S_S,
	asm_r5.rates.X_I ~ reactor5.rates.X_I,
	asm_r5.rates.X_S ~ reactor5.rates.X_S,
	asm_r5.rates.X_BH ~ reactor5.rates.X_BH,
	asm_r5.rates.X_BA ~ reactor5.rates.X_BA,
	asm_r5.rates.X_P ~ reactor5.rates.X_P,
	asm_r5.rates.S_NO ~ reactor5.rates.S_NO,
	asm_r5.rates.S_ND ~ reactor5.rates.S_ND,
	asm_r5.rates.X_ND ~ reactor5.rates.X_ND,
	asm_r5.rates.S_ALK ~ reactor5.rates.S_ALK,
	# Rate for S_O (asm + aeration)
	add5.input1.u ~ asm_r5.rates.S_O,
	add5.input2.u ~ aeration5.rates.S_O,
	add5.output.u ~ reactor5.rates.S_O,
	# Rate for S_NH (asm + hybrid process)
#	add5_hyb.input1.u ~ asm_r5.rates.S_NH,
#	add5_hyb.input2.u ~ hybrid_process.rates.S_NH,
#	add5_hyb.output.u ~ reactor5.rates.S_NH,
	asm_r5.rates.S_NH ~ reactor5.rates.S_NH,
	# States to asm, aeration and Hybrid Process
	connect(reactor5.state, asm_r5.state),
	reactor5.state.S_O ~ aeration5.state.S_O,
#	reactor4.state.S_O ~ hybrid_process.S_O,
#	reactor4.state.S_NH ~ hybrid_process.S_NH,
	aeration5.k_La.u ~ 84, # [d^-1] from BSM1 paper, Open-Loop assessment
]
nothing #hide
```

### System Connection
Connecting the systems using the low-level API is quite laborious as every single variable has to be connected on it's own. High-level functions to significantly reduce this effort and make it usable are planned.

```@example lowlevel
## Connect the systems
bsm1_eqs = [  
	# Connect unifier to inflow  
	connect(inflow_q.output, inflow.q);
	connect(inflow_S_I.output, inflow.S_I);
	connect(inflow_S_S.output, inflow.S_S);
	connect(inflow_X_I.output, inflow.X_I);
	connect(inflow_X_S.output, inflow.X_S);
	connect(inflow_X_BH.output, inflow.X_BH);
	connect(inflow_X_BA.output, inflow.X_BA);
	connect(inflow_X_P.output, inflow.X_P);
	connect(inflow_S_O.output, inflow.S_O);
	connect(inflow_S_NO.output, inflow.S_NO);
	connect(inflow_S_NH.output, inflow.S_NH);
	connect(inflow_S_ND.output, inflow.S_ND);
	connect(inflow_X_ND.output, inflow.X_ND);
	connect(inflow_S_ALK.output, inflow.S_ALK);
	connect(inflow.outflow, unifier.inflow_1);
	  
	# Connect Unifier outflow to first reactor  
	connect(unifier.outflow, reactor1.inflow);
	# Internal connections  
	reactor1_eqs;  
	  
	# Connect reactor 1 to reactor 2  
	connect(reactor1.outflow, reactor2.inflow);
	# Internal connections  
	reactor2_eqs;  
	  
	# Connect reactor 2 to reactor 3  
	connect(reactor2.outflow, reactor3.inflow);
	# Internal connections
	reactor3_eqs;
	  
	# Connect reactor 3 to reactor 4  
	connect(reactor3.outflow, reactor4.inflow);
	# Internal connections  
	reactor4_eqs;  
	  
	# Connect reactor 4 to reactor 5  
	connect(reactor4.outflow, reactor5.inflow);
	# Internal connections  
	reactor5_eqs;  
	  
	# Connect reactor 5 to clarifier  
	connect(reactor5.outflow, clarifier.inflow);
	  
	# Connect clarifier to clarifier pump  
	connect(clarifier.sludge, clarifier_pump.inflow);

	# Connect clarifier pump to wastage separator
	connect(clarifier_pump.outflow, wastage_separator.inflow)
	  
	# Connect wastage separator outflow 1 to unifier  
	connect(wastage_separator.outflow_1, unifier.inflow_2);

	# Connect wastage separator outflow 2 to wastage pump
	connect(wastage_separator.outflow_2, wastage_pump.inflow)
]
nothing #hide
```

Then, the overall BSM1 model can be connected and the equations simplified using the `ModelingToolkit.jl`:

```@example lowlevel
@named bsm1_model = ODESystem(
	bsm1_eqs, t,
	systems = [
		inflow_q,
		inflow_S_I,
		inflow_S_S,
		inflow_X_I,
		inflow_X_S,
		inflow_X_BH,
		inflow_X_BA,
		inflow_X_P,
		inflow_S_O,
		inflow_S_NO,
		inflow_S_NH,
		inflow_S_ND,
		inflow_X_ND,
		inflow_S_ALK,
		inflow,
		unifier,
		reactor1,
		asm_r1,
		reactor2,
		asm_r2,
		reactor3,
		asm_r3,
		aeration3,
		add3,
		reactor4,
		asm_r4,
		aeration4,
		add4,
		reactor5,
		asm_r5,
		aeration5,
		add5,
#		hybrid_process,
#		add5_hyb,
		clarifier,
		clarifier_pump,
		wastage_separator,
		wastage_pump
	]
)
sys = structural_simplify(bsm1_model)
nothing #hide
```

### Hybrid Model Training
Once the model is built, the hybrid model has to be trained. For this the [Optimization.jl](https://docs.sciml.ai/Optimization/stable/) suite is used.

```@meta
# Use
# @example lowlevel
# instead of just
# julia
# in the line below for execution. Commenting out produces a documenter error
```
```julia
# Load the output data
output_data = CSV.File(output_data_filename) |> DataFrame

# Train the model
# Function to run model
function run_model(t_obs, params, init; kwargs...)
	tspan = (t_obs[1], t_obs[end])
	prob = ODAEProblem(sys, init, tspan, params; kwargs...)
	sol = solve(
		prob,
		Tsit5(),
		saveat = t_obs,
		sensealg = BacksolveAdjoint(autojacvec=EnzymeVJP())
	)
end
# define loss fcn
function compute_loss(p)
	pred = run_model(output_data.t, p, c_init)
	sqrt(mean(abs2, Array(pred(output_data.t))[:S_NH,:] .- output_data.S_NH))
end  
# Optimization Callback
optim_callback = function (p, l)
	println(l)
	return false
end

# Calculate gradients (either of those)
ForwardDiff.gradient(compute_loss, p_nn)
#ReverseDiff.gradient(compute_loss, p_nn)
#Zygote.gradient(compute_loss, p_nn)

# Optimize
optf = Optimization.OptimizationFunction(
	(θ, p) -> compute_loss(θ), 
	Optimization.AutoForwardDiff()
)
optprob = Optimization.OptimizationProblem(optf, p_nn)
sol = Optimization.solve(
	optprob, 
	OptimizationOptimisers.Adam(0.0001), 
	callback = optim_callback, 
	maxiters = 5
)
p_nn = sol.u
nothing #hide
```

### Simulation and Plotting
With the optimized parameters, then the system can be simulated and the output plotted.

```@example lowlevel
## Simulate the system (1 to 10 days)
#prob = ODEProblem(sys, p_nn, (0, 10))
prob = ODEProblem(sys, [], (0, 10), [])
sol = solve(prob)

## Plot the ouput
plot(sol, 
	idxs = states(bsm1_model)[occursin.("clarifier₊effluent", string.(states(bsm1_model)))][2:end], # States which are associated with effluent without flow
	title = "BSM1 Effluent",
)
```

## High-Level API

This is a first draft of the intended high-level interface.
This aims to provide a more convenient and practically usable interface to the low-level API. Note that this is as well still under development and changes are expected. It will be implemented only after the low-level API works and build on it.

### Setup

First, the imports and parameter definitions have to be done similarly to the low-level API.

```@example highlevel
# Import the needed packages
using BioChemicalTreatment
using ModelingToolkit
using ModelingToolkitStandardLibrary.Blocks: Constant, Add
using Plots
using ComponentArrays
using ForwardDiff
using Optimization, OptimizationOptimisers
using Lux, Random
using CSV, DataFrames

## Load data
output_data_filename = "output_data.csv"

## Parameters
# ASM Parameters
params = Dict(
	:Y_A => 0.24, # autotrophic yield
	:Y_H => 0.67, # heterotrophic yield
	:f_P => 0.08, # fraction of biomass yielding particulate procucts
	:i_XB => 0.08, # mass N / mass COD in biomass
	:i_XP => 0.06, # mass N / mass COD in products from biomass
	:μ_H => 4.0, # Heterotrophic growth and decay
	:K_S => 10.0,
	:K_OH => 0.2,
	:K_NO => 0.5,
	:b_H => 0.3,
	:η_g => 0.8, # correction factor for anoxic growth of heterotrophs
	:η_h => 0.8, # correction factor for anoxic hydrolysis
	:k_h => 3.0, # hydrolysis
	:K_X => 0.1,
	:μ_A => 0.5, # Autotrophic growth and decay
	:K_NH => 1.0,
	:b_A => 0.05,
	:K_OA => 0.4,
	:k_a => 0.05, # ammonification
)
params_aeration = Dict(
	:kₗa => 240 # oxygen transfer coefficient [d^-1]
)

# Volumes
V_na = 1000 # Volume [m^3]
V_a = 1333 # Volume [m^3]

# Initial conditions
c_init = Dict(
	:S_I => 1.0,
	:S_S => 1.0,
	:X_I => 1.0,
	:X_S => 1.0,
	:X_BH => 1.0,
	:X_BA => 1.0,
	:X_P => 1.0,
	:S_O => 1.0,
	:S_NO => 1.0,
	:S_NH => 1.0,
	:S_ND => 1.0,
	:X_ND => 1.0,
	:S_ALK => 1.0
)
```

### System Creation

The systems are created similarly to the low-level interface, but the processes are directly passed to the reactors, which then are in charge of adding them. Here as well the user gets full control over the added hybrid process.

```@example highlevel
## Build up model parts
# Time variable
@variables t
# Processes
@named aeration = Aeration(;params_aeration...)
# Hybrid process
rng = Random.default_rng()
NN = Lux.Chain(Lux.Dense(2, 1, tanh))
p_nn_init = Lux.setup(rng, NN)
p_nn = ComponentVector(nn_weights = p_NN_init)
@named hybrid_process = HybridProcess(NN, variables_in = [:S_O, :S_NH], variables_out = [:S_NH])
# ASM
@named asm = ASM1(;params...)

# Inflow
@named inflow = Constant(k = [1000; c_init]) # Inflow: flowrate and concentrations

# Flow elements
@named clarifier = IdealClarifier(states = states(asm)) # Determine which are solid etc over variable annotations
@named unifier = FlowUnifier(states = states(asm); nflows=2)
@named wastage_pump = FlowSeparator(states = states(asm); nflows=2)

# Reactors, first two are non-aerated
@named reactor1 = CSTR(
	v = V_na,
	states = states(asm),
	inital_state = c_init,
	processes = [asm]
)
@named reactor2 = CSTR(
	v = V_na,
	states = states(asm),
	initial_state = c_init,
	processes = [asm]
)
@named reactor3 = CSTR(
	v = V_a,
	states = states(asm),
	initial_state = c_init,
	processes = [asm, aeration]
)
@named reactor4 = CSTR(
	v = V_a,
	states = states(asm),
	initial_state = c_init,
	processes = [asm, aeration]
)
@named reactor5 = CSTR(
	v = V_a,
	states = states(asm),
	initial_state = c_init,
	processes = [asm, aeration, hybrid_process]
)
```

### System Connections

Connecting the systems is easier as well, all inflows and outflows can be connected as one:

```@example highlevel
## Connect the systems
bsm1_eqs = [
	connect(inflow.output, unifier.inflow1),
	connect(unifier.outflow, reactor1.inflow),
	connect(reactor1.outflow, reactor2.inflow),
	connect(reactor2.outflow, reactor3.inflow),
	connect(reactor3.outflow, reactor4.inflow),
	connect(reactor4.outflow, reactor5.inflow),
	connect(reactor5.outflow, clarifier.inflow),
	connect(clarifier.sludge_overflow, wastage_pump.inflow),
	connect(wastage_pump.outflow1, unifier.inflow2)
]
```

Constructing the model and simplifying it gets as well easier as only the overall systems have to be added.

```@example highlevel
@named bsm1_model = ODESystem(
	bsm1_eqs,
	t,
	systems = [
		inflow,
		unifier,
		reactor1,
		reactor2,
		reactor3,
		reactor4,
		reactor5,
		clarifier,
		wastage_pump
	]
)
sys = structural_simplify(bsm1_model)
```

### Hybrid Model Training

Once the model is built, training the hybrid model is again similar to the low-level API.

```@example highlevel
# Load the output data
output_data = CSV.File(output_data_filename) |> DataFrame

# Train the model
# Function to run model
function run_model(t_obs, params, init; kwargs...)
	tspan = (t_obs[1], t_obs[end])
	prob = ODAEProblem(sys, init, tspan, params; kwargs...)
	sol = solve(
		prob, 
		Tsit5(), 
		saveat = t_obs,
		sensealg = BacksolveAdjoint(autojacvec=EnzymeVJP())
	)
end
# define loss fcn
function compute_loss(p)
	pred = run_model(output_data.t, p, c_init)
	sqrt(mean(abs2, Array(pred(output_data.t))[:S_NH,:] .- output_data.S_NH))
end  
# Optimization Callback
optim_callback = function (p, l)
	println(l)
	return false
end

# Calculate gradients (either of those)
ForwardDiff.gradient(compute_loss, p_nn)
#ReverseDiff.gradient(compute_loss, p_nn)
#Zygote.gradient(compute_loss, p_nn)

# Optimize
optf = Optimization.OptimizationFunction(
	(θ, p) -> compute_loss(θ), 
	Optimization.AutoForwardDiff()
)
optprob = Optimization.OptimizationProblem(optf, p_nn)
sol = Optimization.solve(
	optprob, 
	OptimizationOptimisers.Adam(0.0001), 
	callback = optim_callback, 
	maxiters = 5
)
p_nn = sol.u
```

### Simulation and Plotting

Simulation and plotting with the optimized parameters works again similarly to the low-level interface.

```@example highlevel
## Simulate the system (1 to 10 days)
prob = ODAEProblem(sys, p_nn, (0, 10))
sol = solve(prob, Tsit5())

## Plot the ouput
plot(sol, 
	idxs = [clarifier.effluent],
	title = "BSM1 Effluent",
)
```
