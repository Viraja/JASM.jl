"""
    symbolic_to_namestring(sym)

A helper function to get the name of a symbol as string. Removes all namespacing and dependencies.
"""
function symbolic_to_namestring(sym::SymbolicUtils.Symbolic)::String
    # Remove namespacing
    symbol = symbolic_to_stringparts(sym)[end]
    # Remove dependencies
    split(symbol, '(')[1]
end
function symbolic_to_namestring(sym::Symbolics.Num)::String
    symbolic_to_namestring(Symbolics.unwrap(sym))
end

"""
    symbolic_to_stringparts(sym)

Get the parts of a symbol as strings.
"""
function symbolic_to_stringparts(sym::SymbolicUtils.Symbolic)::Vector{SubString{String}}
    # split namespacing
    split(string(sym), "₊")
end
function symbolic_to_stringparts(sym::Symbolics.Num)::Vector{SubString{String}}
    # split namespacing
    symbolic_to_stringparts(Symbolics.unwrap(sym))
end


"""
    ic_to_array(vars, initial_conditions)

A helper function to transform the initial_conditions to a array.
Supports a array or variable maps (keys as Symbols, Strings or Symbolic variables)
"""
function ic_to_array(vars::AbstractArray{<:AbstractString}, initial_conditions)
    # If the initial conditions are given as dict or vector of pairs
    if eltype(initial_conditions) <: Pair
        # Make initial conditions a dict if not yet
        initial_conditions = initial_conditions isa Dict ? initial_conditions : Dict(initial_conditions)
        # Make keys to be strings (if variable remove namespacing, otherwise simply convert to string)
        initial_conditions = Dict(zip([isa(k, SymbolicUtils.Symbolic) || isa(k, Symbolics.Num) ? symbolic_to_namestring(k) : string(k) for k in Symbolics.value.(keys(initial_conditions))], values(initial_conditions)))
    end
    # Use ModelingToolkit.varmap_to_vars to get correct order and set ic to zero where not given
    ModelingToolkit.varmap_to_vars(initial_conditions, vars; defaults = Dict(zip(vars, zeros(length(vars)))))
end


"""
    is_rate(st)

Whether the state corresponds to a rates subsystem.
"""
is_rate(st) = "rates" in symbolic_to_stringparts(st)[1:end-1]

"""
    get_states(model)

Get the states of the model that do not belong to the "rates" subsystem.
"""
get_states(model) = [st for st in states(model) if !is_rate(st)]

"""
    get_rates(model)

Get the states of the model that belong to the "rates" subsystem.
"""
get_rates(model) = [st for st in states(model) if is_rate(st)]

"""
    get_rate_equations()

Gets the rate equations from a model in the same order as the get_rates function.
"""
function get_rate_equations(model)
   rates = get_rates(model)
   eqs_ = equations(model)
   # gets the left-hand-side of all equations of the model
   lhs_ = [e.lhs for e in eqs_]
   # indexin(target, ref), where target is the order we want to get and ref is the one that needs to be ordered
   order = indexin(rates, lhs_)
   # returns the equations in the new order according to get_rates
   return eqs_[order]
end

"""
    split_states(model)

Split the states of the model on those that belong to the "rates" subsystem and those that do not.
"""
function split_states(model)
   rates = []
   states_ = []
   for st in states(model)
      if is_rate(st)
         push!(rates, st)
      else
         push!(states_, st)
      end
   end

   # cannot assume that sorting matches
   order = indexin(symbolic_to_namestring.(states_), symbolic_to_namestring.(rates))
   return states_, rates[order]
end

"""
    flatten(x)
Changes the Julia internal flatten function.
"""
flatten(x) = collect(Iterators.flatten(x))

"""
    rates_to_ode(model; name=nothing)

Convert a model defining an algebraic system of rates to a system of ODEs.

If `name` is unspecified, inherit the name from model.

"""
function rates_to_ode(model; name=nothing)
    # get states (from states subsystem), rate equations and independent variable
    states_ = get_states(model)
    # instead of equations(model) as this might have a different order from get_states()
    rates = get_rate_equations(model)

    # remove subsystems in states and rates
    # 1. create new variables from given state name. Nicer way? i.e. not using flatten
    t = ModelingToolkit.get_iv(model)
    new_st = flatten(@variables $st(t) for st in Symbol.(symbolic_to_namestring.(states_)))
    # 2. substitute the states with subsystems in the RHS of the rates
    subs = [st => nst for (st, nst) in zip(states_, new_st)]
    rates = [Symbolics.substitute(r.rhs, subs) for r in rates]

    # Write the set of ODEs inheriting parameters and defaults from model
    D = Differential(t)
    eqs = [D(s) ~ r for (s,r) in zip(new_st, rates)]
    ODESystem(eqs, t, new_st, parameters(model), defaults=model.defaults;
              name=isnothing(name) ? model.name : name)

end

