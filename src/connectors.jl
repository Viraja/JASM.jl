"""
    InflowPort(components; initial_flow, initial_concentrations, name)

A connector for connecting inflows carrying components.

# Parameters:
    - `components`: The components in the flow. Can be provided as:
        - A `Dict` with keys being Strings representing the names of the components in the flow and values being Boolean meaning if the component is particulate or not. Does not include the flow rate.
        - An array of symbols for the components. Name and if particulate or not are taken from these symbols. May or may not include the the flow rate `q`.
    - `initial_flow`: The initial value for the flow rate `q`. Defaults to 0.
    - `initial_concentrations`: Initial concentrations of the components in the flow, supports array and variable map (keys can be string, symbol or symbolic variables). Defaults to 0 for each component or the default values of the symbols if given as symbols.

# States:
    - `q`: The flow rate
    - A state for each component specified by the `components` parameter.
"""
@connector function InflowPort(components::Dict{<:AbstractString, Bool}; initial_flow = 0.0, initial_concentrations = zeros(length(components)), name)
    # Support initial concentrations in all formats that ModelingToolkit supports
    initial_concentrations = ic_to_array(collect(keys(components)), initial_concentrations)
    # Set the initial flow to 0 if not given
    if isnothing(initial_flow)
        initial_flow = 0.0
    end

    # Time as the IndependentVariable
    @variables t

    # Flow state
    states = @variables q(t) = initial_flow [
        input = true,
        description = "Flow Rate"
    ]

    # Sort the components as otherwise the connect() sometimes connects wrong components
    p = sortperm(collect(components), by = x->x[1])
    # State for each component: Add particulate and description metadata for each
    for (component, c_init) = zip(collect(components)[p], initial_concentrations[p])
        symb = Symbol(component[1])
        state = @variables $symb(t) = c_init [
            input = true,
            particulate = component[2],
            description = component[1]*" concentation"
        ]
        append!(states, state)
    end

    ODESystem(Equation[], t, states, []; name = name)
end
@connector function InflowPort(components::AbstractArray{<:SymbolicUtils.Symbolic}; initial_flow = nothing, initial_concentrations = nothing, name)
    # Get if any component is the flow rate (Distinguished by not having an isparticulate attribute)
    flow_connect = isnothing.(BioChemicalTreatment.isparticulate.(components; default = nothing))
    if sum(flow_connect) > 1
        error("All components in the flow must have the particulate attribute (One without is accepted and considered as the flow rate)")
    end
    # Get the initial flow if not given (from the symbolic variable default or 0)
    if isnothing(initial_flow)
        initial_flow = any(flow_connect) ? Symbolics.getdefaultval(components[flow_connect][1], 0.0) : 0.0
    end
    # Sort the flow rate out from components
    components = components[.!flow_connect]
    # Get the initial concentrations if not given (from symbolic variable default or 0)
    if isnothing(initial_concentrations)
        initial_concentrations = Symbolics.getdefaultval.(components, 0)
    end
    # Transform the initial condition to a map to ensure ordering
    if !(eltype(initial_concentrations) <: Pair)
        initial_concentrations = Dict(zip(components, initial_concentrations))
    end

    # Get the names of the components and if they are particulate, then combine them to a dict
    names = symbolic_to_namestring.(components)
    particulate = BioChemicalTreatment.isparticulate.(components)
    comp_dict = Dict(zip(names, particulate))

    InflowPort(comp_dict; initial_flow, initial_concentrations, name)
end
@connector function InflowPort(components::AbstractArray{<:Symbolics.Num}; initial_flow = nothing, initial_concentrations = nothing, name)
    # Unwrap the Num to a Symbolic and forward it
    if isnothing(initial_concentrations)
        InflowPort(Symbolics.value.(components); initial_flow, name)
    else
        InflowPort(Symbolics.value.(components); initial_flow, initial_concentrations, name)
    end
end

"""
    OutflowPort(components; initial_flow, initial_concentrations, name)

A connector for connecting outflows carrying components.

# Parameters:
    - `components`: The components in the flow. Can be provided as:
        - A `Dict` with keys being Strings representing the names of the components in the flow and values being Boolean meaning if the component is particulate or not. Does not include the flow rate.
        - An array of symbols for the components. Name and if particulate or not are taken from these symbols. May or may not include the the flow rate `q`.
    - `initial_flow`: The initial value for the flow rate `q`. Defaults to 0.
    - `initial_concentrations`: Initial concentrations of the components in the flow, supports array and variable map (keys can be string, symbol or symbolic variables). Defaults to 0 for each component or the default values of the symbols if given as symbols.

# States:
    - `q`: The flow rate
    - A state for each component specified by the `components` parameter.
"""
@connector function OutflowPort(components::Dict{<:AbstractString, Bool}; initial_flow = 0.0, initial_concentrations = zeros(length(components)), name)
    # Support initial concentrations in all formats that ModelingToolkit supports
    initial_concentrations = ic_to_array(collect(keys(components)), initial_concentrations)
    # Set the initial flow to 0 if not given
    if isnothing(initial_flow)
        initial_flow = 0.0
    end
    
    # Time as the IndependentVariable
    @variables t

    # Flow state 
    states = @variables q(t) = initial_flow [
        output = true,
        description = "Flow Rate"
    ]

    # Sort the components as otherwise the connect() sometimes connects wrong components
    p = sortperm(collect(components), by = x->x[1])
    # State for each component: Add particulate and description metadata for each
    for (component, c_init) = zip(collect(components)[p], initial_concentrations[p])
        symb = Symbol(component[1])
        state = @variables $symb(t) = c_init [
            output = true,
            particulate = component[2],
            description = component[1]*" concentation"
        ]
        append!(states, state)
    end

    ODESystem(Equation[], t, states, []; name = name)
end
@connector function OutflowPort(components::AbstractArray{<:SymbolicUtils.Symbolic}; initial_flow = nothing, initial_concentrations = nothing, name)
    # Get if any component is the flow rate (Distinguished by not having an isparticulate attribute)
    flow_connect = isnothing.(BioChemicalTreatment.isparticulate.(components; default = nothing))
    if sum(flow_connect) > 1
        error("All components in the flow must have the particulate attribute (One without is accepted and considered as the flow rate)")
    end
    # Get the initial flow if not given (from the symbolic variable default or 0)
    if isnothing(initial_flow)
        initial_flow = any(flow_connect) ? Symbolics.getdefaultval(components[flow_connect][1], 0.0) : 0.0
    end
    # Sort the flow rate out
    components = components[.!flow_connect]
    # Get the initial concentrations if not given (from symbolic variable default or 0)
    if isnothing(initial_concentrations)
        initial_concentrations = Symbolics.getdefaultval.(components, 0)
    end
    # Transform the initial condition to a map to ensure ordering
    if !(eltype(initial_concentrations) <: Pair)
        initial_concentrations = Dict(zip(components, initial_concentrations))
    end


    # Get the names of the components and if they are particulate, then combine them to a dict
    names = symbolic_to_namestring.(components)
    particulate = BioChemicalTreatment.isparticulate.(components)
    comp_dict = Dict(zip(names, particulate))

    OutflowPort(comp_dict; initial_flow, initial_concentrations, name)
end
@connector function OutflowPort(components::AbstractArray{<:Symbolics.Num}; initial_flow = nothing, initial_concentrations = nothing, name)
    # Unwrap the Num to a Symbolic and forward it
    if isnothing(initial_concentrations)
        OutflowPort(Symbolics.value.(components); initial_flow, name)
    else 
        OutflowPort(Symbolics.value.(components); initial_flow, initial_concentrations, name)
    end
end


"""
    ReactionInputPort(components; initial_rates, name)

A connector for connecting reaction rates as input.

# Parameters:
    - `components`: The components. Can be provided as:
        - An array of Strings for the component names.
        - An array of symbols for the components, from which the names are taken.
    - `initial_rates`: The initial reaction rates, supports array and variable map (keys can be string, symbol or symbolic variables). Defaults to 0 for all rates or the default value if given as symbolic.

# States:
    - A state for each reaction rate specified by the `components` parameter.
"""
@connector function ReactionInputPort(components::AbstractArray{<:AbstractString}; initial_rates = zeros(length(components)), name)
    # Support initial rates in all formats that ModelingToolkit supports
    initial_rates = ic_to_array(components, initial_rates)

    # Time as the IndependentVariable
    @variables t

    # Vector for taking up all the states
    states = Symbolics.Num[]

    # Sort the components as otherwise the connect() sometimes connects wrong components
    p = sortperm(collect(components), by = x->x[1])
    # State for each component: Add description metadata for each
    for (component, init) = zip(collect(components)[p], initial_rates[p])
        symb = Symbol(component)
        state = @variables $symb(t) = init [
            input = true,
            description = component*" reaction rate"
        ]
        append!(states, state)
    end

    ODESystem(Equation[], t, states, []; name = name)
end
@connector function ReactionInputPort(components::AbstractArray{<:SymbolicUtils.Symbolic}; initial_rates = Symbolics.getdefaultval.(components, 0), name)
    # Get the component rates and forward it
    names = symbolic_to_namestring.(components)
    # Transform the initial condition to a map to ensure ordering
    if !(eltype(initial_rates) <: Pair)
        initial_rates = Dict(zip(components, initial_rates))
    end

    ReactionInputPort(names; initial_rates, name)
end
@connector function ReactionInputPort(components::AbstractArray{<:Symbolics.Num}; initial_rates = nothing, name)
    # Unwrap the components to a Symbolic and forward it
    if isnothing(initial_rates)
        ReactionInputPort(Symbolics.value.(components); name)
    else 
        ReactionInputPort(Symbolics.value.(components); initial_rates, name)
    end
end


"""
    ReactionOutputPort(components; initial_rates, name)

A connector for connecting reaction rates as output.

# Parameters:
    - `components`: The components. Can be provided as:
        - An array of Strings for the component names.
        - An array of symbols for the components, from which the names are taken.
    - `initial_rates`: The initial reaction rates, supports array and variable map (keys can be string, symbol or symbolic variables). Defaults to 0 for all rates or the default value if given as symbolic.

# States:
    - A state for each reaction rate specified by the `components` parameter.
"""
@connector function ReactionOutputPort(components::AbstractArray{<:AbstractString}; initial_rates = zeros(length(components)), name)
    # Support initial rates in all formats that ModelingToolkit supports
    initial_rates = ic_to_array(components, initial_rates)

    # Time as the IndependentVariable
    @variables t

    # Vector to be filled with the states
    states = Symbolics.Num[]

    # Sort the components as otherwise the connect() sometimes connects wrong components
    p = sortperm(collect(components), by = x->x[1])
    # State for each component: Add description metadata for each
    for (component, init) = zip(collect(components)[p], initial_rates[p])
        symb = Symbol(component)
        state = @variables $symb(t) = init [
            output = true,
            description = component*" reaction rate"
        ]
        append!(states, state)
    end

    ODESystem(Equation[], t, states, []; name = name)
end
@connector function ReactionOutputPort(components::AbstractArray{<:SymbolicUtils.Symbolic}; initial_rates = Symbolics.getdefaultval.(components, 0), name)
    # Get the component names and forward them
    names = symbolic_to_namestring.(components)
    # Transform the initial condition to a map to ensure ordering
    if !(eltype(initial_rates) <: Pair)
        initial_rates = Dict(zip(components, initial_rates))
    end

    ReactionOutputPort(names; initial_rates, name)
end
@connector function ReactionOutputPort(components::AbstractArray{<:Symbolics.Num}; initial_rates = nothing, name)
    # Unwrap the nums and forward them
    if isnothing(initial_rates)
        ReactionOutputPort(Symbolics.value.(components); name)
    else 
        ReactionOutputPort(Symbolics.value.(components); initial_rates, name)
    end
end


"""
    StateInputPort(states; initial_states, name)

A connector for connecting reactor states as input.

# Parameters:
    - `states`: The states. Can be provided as:
        - A `Dict` with keys being Strings representing the names of the states and values being Boolean or nothing meaning if the component is particulate or not (or not a component).
        - An array of symbols for the components. Name and if particulate or not are taken from these symbols.
    - `initial_states`: Initial states, supports array and variable map (keys can be string, symbol or symbolic variables). Defaults to 0 for each state or the default values of the symbols if given as symbols.

# States:
    - A state for each state specified by the `states` parameter
"""
@connector function StateInputPort(states::Dict{<:AbstractString, <:Union{Bool, Nothing}}; initial_states = zeros(length(states)), name)
    # Support initial rates in all formats that ModelingToolkit supports
    initial_states = ic_to_array(collect(keys(states)), initial_states)

    # Time as the IndependentVariable
    @variables t

    # Vector to be filled with the states
    vars = Symbolics.Num[]

    # Sort the states as otherwise the connect() sometimes connects wrong states
    p = sortperm(collect(states), by = x->x[1])
    # Add the states, with the description metadata and the particulate if given.
    for (state, init) = zip(collect(states)[p], initial_states[p])
        symb = Symbol(state[1])
        if !isnothing(state[2])
            s = @variables $symb(t) = init [
                input = true,
                particulate = state[2],
                description = state[1]
            ]
        else
            s = @variables $symb(t) = init [
                input = true,
                description = state[1]
            ]
        end
        append!(vars, s)
    end

    ODESystem(Equation[], t, vars, []; name = name)
end
@connector function StateInputPort(states::AbstractArray{<:SymbolicUtils.Symbolic}; initial_states = Symbolics.getdefaultval.(states, 0), name)
    # Get the names and isparticulate of the given states and assemble it to a dict
    names = symbolic_to_namestring.(states)
    particulate = BioChemicalTreatment.isparticulate.(states; default = nothing)
    state_dict = Dict(zip(names, particulate))

    # Transform the initial condition to a map to ensure ordering
    if !(eltype(initial_states) <: Pair)
        initial_states = Dict(zip(states, initial_states))
    end

    StateInputPort(state_dict; initial_states, name)
end
@connector function StateInputPort(states::AbstractArray{<:Symbolics.Num}; initial_states = nothing, name)
    # Unwrap the Nums and forward them
    if isnothing(initial_states)
        StateInputPort(Symbolics.value.(states); name)
    else 
        StateInputPort(Symbolics.value.(states); initial_states, name)
    end
end


"""
    StateOutputPort(states; initial_states, name)

A connector for connecting reactor states as output.

# Parameters
    - `states`: The states. Can be provided as:
        - A `Dict` with keys being Strings representing the names of the states and values being Boolean or nothing meaning if the component is particulate or not (or not a component).
        - An array of symbols for the components. Name and if particulate or not are taken from these symbols.
    - `initial_states`: Initial states, supports array and variable map (keys can be string, symbol or symbolic variables). Defaults to 0 for each state or the default values of the symbols if given as symbols.

# States:
    - A state for each state specified by the `states` parameter
"""
@connector function StateOutputPort(states::Dict{<:AbstractString, <:Union{Bool, Nothing}}; initial_states = zeros(length(states)), name)
    # Support initial rates as array or map
    initial_states = ic_to_array(collect(keys(states)), initial_states)

    # Time as the IndependentVariable
    @variables t

    # Vector to be filled with the state symbols
    vars = Symbolics.Num[]

    # Sort the states as otherwise the connect() sometimes connects wrong states
    p = sortperm(collect(states), by = x->x[1])
    # Add the states, with the description metadata and the particulate if given.
    for (state, init) = zip(collect(states)[p], initial_states[p])
        symb = Symbol(state[1])
        if !isnothing(state[2])
            s = @variables $symb(t) = init [
                output = true,
                particulate = state[2],
                description = state[1]
            ]
        else
            s = @variables $symb(t) = init [
                output = true,
                description = state[1]
            ]
        end
        append!(vars, s)
    end

    ODESystem(Equation[], t, vars, []; name = name)
end
@connector function StateOutputPort(states::AbstractArray{<:SymbolicUtils.Symbolic}; initial_states = Symbolics.getdefaultval.(states, 0), name)
    # Get the name and isparticulate of the symbols and assemble to a dict
    names = symbolic_to_namestring.(states)
    particulate = BioChemicalTreatment.isparticulate.(states)
    state_dict = Dict(zip(names, particulate))

    # Transform the initial condition to a map to ensure ordering
    if !(eltype(initial_states) <: Pair)
        initial_states = Dict(zip(states, initial_states))
    end

    StateOutputPort(state_dict; initial_states, name)
end
@connector function StateOutputPort(states::AbstractArray{<:Symbolics.Num}; initial_states = nothing, name)
    # Unwrap the Nums and forward it
    if isnothing(initial_states)
        StateOutputPort(Symbolics.value.(states); name)
    else 
        StateOutputPort(Symbolics.value.(states); initial_states, name)
    end
end

