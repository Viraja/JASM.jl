# Copyright (C) 2023 Juan Pablo Carbajal
# Copyright (C) 2023 Florian Wenk
# Copyright (C) 2023 Mariane Yvonne Schneider
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Author: Florian Wenk <florian.wenk@eawag.ch>
# Author: Mariane Yvonne Schneider <myschneider@meiru.ch>
# Date: 07.07.2023



module BioChemicalTreatment

using ModelingToolkit
using ModelingToolkitStandardLibrary

include("utils.jl")

export monod_moser
include("growth.jl")

export isparticulate
include("variables.jl")

export InflowPort, OutflowPort, ReactionInputPort, ReactionOutputPort, StateInputPort, StateOutputPort
include("connectors.jl")

export CSTR
include("reactors.jl")
# Reactions

export ASM1
include("asm1.jl")

export OzoneDisinfection
include("disinfection.jl")

export InflowPortToRealOutput, RealInputToOutflowPort
export ReactionInputToRealOutput, RealInputToReactionOutput
export StateInputToRealOutput, RealInputToStateOutput
include("modelingtoolkitstandardlibrarycompat.jl")

export FlowConnector, FlowUnifier, FlowSeparator
export FlowPump
include("flowelements.jl")

export IdealClarifier
include("clarifiers.jl")

export Aeration
include("aeration.jl")

welcome() = println("Welcome to BioChemicalTreatment")
# Write your package code here.

end
