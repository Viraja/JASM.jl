"""
    realports_from_combinedport(port::ODESystem; name, output_port = false)

Helper function for StandardLibrary Ports compatibility: Generates a RealInput/RealOutput for each state in the port.
Returns a ODESystem with the equations for setting the new ports and the corresponding state equal and the newly generated In-/Outputs
"""
function realports_from_combinedport(port::ODESystem; name, output_port = false)
    # Get Port Type for the Real ports (RealInput or RealOutput)
    if output_port
        PortType = ModelingToolkitStandardLibrary.Blocks.RealOutput
    else
        PortType = ModelingToolkitStandardLibrary.Blocks.RealInput
    end
    # Vectors for the ModelingToolkitStandardLibrary Ports and the equations
    ports = ODESystem[]
    eqns = Equation[]

    # Time as independent variable
    @variables t

    # Generate the ModelingToolkitStandardLibrary Ports and the equations (all states from the combined port are equal to the new Single port)
    for state in states(port)
        push!(ports, PortType(;name = Symbol(BioChemicalTreatment.symbolic_to_namestring(state)), u_start = Symbolics.getdefaultval(state)))
        push!(eqns, ports[end].u ~ getproperty(port, Symbol(BioChemicalTreatment.symbolic_to_namestring(state))))
    end

    # Return a ODESystem for the combination and the vector with the ports
    ODESystem(eqns, t, [], []; name), ports
end

"""
    RealInputToOutflowPort(components; initial_flow, initial_concentrations, name)

A system which connects to a [`FlowPort`](@ref) and to a series of [`ModelingToolkitStandardLibrary.Blocks.RealInput`s](@ref) on the other sides for compatibility with the ModelingToolkitStandardLibrary.
The system ensures equality between the corresponding symbols. 
Takes the same inputs as [`FlowPort`](@ref).

!!! info
    This function needs the [`ModelingToolkitStandardLibrary`](@ref) to be loaded, as it is only useful with this library.

# Connectors
- `outflow`: The [`OutflowPort`](@ref) to connect
- `q`: A [`ModelingToolkitStandardLibrary.Blocks.RealInput`](@ref) for the flow rate
- One [`ModelingToolkitStandardLibrary.Blocks.RealInput`](@ref) for each component in the flow
"""
@component function RealInputToOutflowPort(components; initial_flow = nothing, initial_concentrations = nothing, name)
    # Start with the OutflowPort and construct the RealInputs based on this -> components can be provided in any format supported by FlowPort
    # Create the OutflowPort
    @named outflow = OutflowPort(components; initial_flow, initial_concentrations)

    # Get the system with the equality equations and the individual ports
    sys, ports = realports_from_combinedport(outflow; output_port = false, name)

    # Compose and return the system
    compose(sys, [outflow, ports...])
end

"""
    InflowPortToRealOutput(components; initial_flow, initial_concentrations, name)

A system which connects to a [`FlowPort`](@ref) and to a series of [`ModelingToolkitStandardLibrary.Blocks.RealOutput`s](@ref) on the other sides for compatibility with the ModelingToolkitStandardLibrary.
Internally the equations ensure equality inbetween the corresponding symbols. Takes the same inputs as [`FlowPort`](@ref).

!!! info
    This function needs the [`ModelingToolkitStandardLibrary`](@ref) to be loaded, as it is only useful with this library.

# Connectors
- `inflow`: The [`InflowPort`](@ref) to connect
- `q`: A [`ModelingToolkitStandardLibrary.Blocks.RealOutput`](@ref) for the flow rate
- One [`ModelingToolkitStandardLibrary.Blocks.RealOutput`](@ref) for each component in the flow
"""
@component function InflowPortToRealOutput(components; initial_flow = nothing, initial_concentrations = nothing, name)
    # Start with the InflowPort and construct the RealOutputs based on this -> components can be provided in any format supported by FlowPort
    # Create the InflowPort
    @named inflow = InflowPort(components; initial_flow, initial_concentrations)

    # Get the system with the equality equations and the individual ports
    sys, ports = realports_from_combinedport(inflow; output_port = true, name)

    # Compose and return the system
    compose(sys, [inflow, ports...])
end


"""
    RealInputToReactionOutput(components; initial_rates, name)

A system which connects to a [`ReactionOutputPort`](@ref) and to a series of [`ModelingToolkitStandardLibrary.Blocks.RealInput`s](@ref) on the other sides for compatibility with the ModelingToolkitStandardLibrary.
The system ensures equality between the corresponding symbols. 
Takes the same inputs as [`ReactionOutputPort`](@ref).

!!! info
    This function needs the [`ModelingToolkitStandardLibrary`](@ref) to be loaded, as it is only useful with this library.

# Connectors
- `reactionoutput`: The [`ReactionOutputPort`](@ref) to connect
- One [`ModelingToolkitStandardLibrary.Blocks.RealInput`](@ref) for each component
"""
@component function RealInputToReactionOutput(components; initial_rates = nothing, name)
    # Start with the ReactionOutputPort and construct the RealInputs based on this -> components can be provided in any format supported by ReactionInputPort
    # Create the ReactionInputPort
    @named reactionoutput = ReactionOutputPort(components; initial_rates)

    # Get the system with the equality equations and the individual ports
    sys, ports = realports_from_combinedport(reactionoutput; output_port = false, name)

    # Compose and return the system
    compose(sys, [reactionoutput, ports...])
end

"""
    ReactionInputToRealOutput(components; initial_rates, name)

A system which connects to a [`ReactionInputPort`](@ref) and to a series of [`ModelingToolkitStandardLibrary.Blocks.RealOutput`s](@ref) on the other sides for compatibility with the ModelingToolkitStandardLibrary.
The system ensures equality between the corresponding symbols. 
Takes the same inputs as [`ReactionInputPort`](@ref).

!!! info
    This function needs the [`ModelingToolkitStandardLibrary`](@ref) to be loaded, as it is only useful with this library.

# Connectors
- `reactioninput`: The [`ReactionInputPort`](@ref) to connect
- One [`ModelingToolkitStandardLibrary.Blocks.RealOutput`](@ref) for each component
"""
@component function ReactionInputToRealOutput(components; initial_rates = nothing, name)
    # Start with the ReactionInputPort and construct the RealOutputs based on this -> components can be provided in any format supported by ReactionInputPort
    # Create the ReactionInputPort
    @named reactioninput = ReactionInputPort(components; initial_rates)

    # Get the system with the equality equations and the individual ports
    sys, ports = realports_from_combinedport(reactioninput; output_port = true, name)

    # Compose and return the system
    compose(sys, [reactioninput, ports...])
end


"""
    RealInputToStateOutput(components; initial_state, name)

A system which connects to a [`StateOutputPort`](@ref) and to a series of [`ModelingToolkitStandardLibrary.Blocks.RealInput`s](@ref) on the other sides for compatibility with the ModelingToolkitStandardLibrary.
The system ensures equality between the corresponding symbols. 
Takes the same inputs as [`StateOutputPort`](@ref).

!!! info
    This function needs the [`ModelingToolkitStandardLibrary`](@ref) to be loaded, as it is only useful with this library.

# Connectors
- `stateoutput`: The [`StateOutputPort`](@ref) to connect
- One [`ModelingToolkitStandardLibrary.Blocks.RealInput`](@ref) for each state
"""
@component function RealInputToStateOutput(states; initial_states = nothing, name)
    # Start with the StateOutputPort and construct the RealInputs based on this -> components can be provided in any format supported by StateOutputPort
    # Create the StateOutputPort
    @named stateoutput = StateOutputPort(states; initial_states)

    # Get the system with the equality equations and the individual ports
    sys, ports = realports_from_combinedport(stateoutput; output_port = false, name)

    # Compose and return the system
    compose(sys, [stateoutput, ports...])
end


"""
    StateInputToRealOutput(components; initial_rates, name)

A system which connects to a [`StateInputPort`](@ref) and to a series of [`ModelingToolkitStandardLibrary.Blocks.RealOutput`s](@ref) on the other sides for compatibility with the ModelingToolkitStandardLibrary.
The system ensures equality between the corresponding symbols. 
Takes the same inputs as [`StateInputPort`](@ref).

!!! info
    This function needs the [`ModelingToolkitStandardLibrary`](@ref) to be loaded, as it is only useful with this library.

# Connectors
- `stateinput`: The [`StateInputPort`](@ref) to connect
- One [`ModelingToolkitStandardLibrary.Blocks.RealOutput`](@ref) for each component
"""
@component function StateInputToRealOutput(states; initial_states = nothing, name)
    # Start with the StateInputPort and construct the RealOutputs based on this -> components can be provided in any format supported by StateInputPort
    # Create the StateInputPort
    @named stateinput = StateInputPort(states; initial_states)

    # Get the system with the equality equations and the individual ports
    sys, ports = realports_from_combinedport(stateinput; output_port = true, name)

    # Compose and return the system
    compose(sys, [stateinput, ports...])
end
