# Copyright (C) 2023 Florian Wenk
# Copyright (C) 2023 Juan Pablo Carbajal
# Copyright (C) 2023 Mariane Yvonne Schneider
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Florian Wenk <florian.wenk@eawag.ch>
# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Author: Mariane Yvonne Schneider <myschneider@meiru.ch>
# Date: 25.07.2023


"""
    ASM1(; name, Y_H=0.67, Y_A=0.24, i_XB=0.086, i_XP=0.01, f_P=0.08, μ_H=6.0, K_S=20.0, K_OH=0.2, K_NO=0.5, b_H=0.62,
         η_g=0.8, μ_A=0.8, K_NH=1.0, K_OA=0.4, b_A=0.1, κ_h=3.0, η_h=0.4, K_X=0.03, κ_A=0.08)

A component that defines the process rates in the activated sludge model number 1 (ASM1). The reference is: Henze, Mogens, Willi Gujer, Takahashi Mino, and M. C. M. van Loosdrecht. Activated Sludge Models ASM1, ASM2, ASM2d and ASM3. IWA Publishing. Londen, UK, 2000.


# Parameters:
  stoichiometric parameter (all at 20 C, neutral pH):
  - `Y_H` heterotrophic yield (default 0.67 gCOD/gCOD)
  - `Y_A` autotrophic yield (default 0.24 gCOD/gCOD)
  - `i_XB` nitrogen fraction in biomass (default 0.086 gN/gCOD)
  - `i_XP` nitrogen fraction in endogenous mass (default 0.01 gN/gCOD)
  - `f_P` fraction of biomass leading to particulate material (default 0.08 -)
  kinetic parameter heterotrophs:
  - `μ_H` maximum specific growth rate (default 6.0 1/day)
  - `K_S` substrate saturation constant (default 20.0 gCOD/m3)
  - `K_OH` Oxygen saturation constant (default 0.2 gO2/m3)
  - `K_NO` nitrate saturation constant (default 0.5 gNO3-N/m3)
  - `b_H` specific decay rate (default 0.62 1/day)
  - `η_g` anoxic growth correction factor (default 0.8 -)
  kinetic parameter autotrophs:
  - `μ_A` maximum specific growth rate (default 0.8 1/day)
  - `K_NH` ammonium saturation constant (default 1.0 gO2/m3)
  - `K_OA` oxygen saturation constant (default 0.4 gO2/m3)
  - `b_A` specific decay rate (default 0.1 1/day)
  hydrolysis parameter
  - `κ_h` maximum specific hydrolysis rate (default 3.0 1/day)
  - `η_h` anoxic hydrolysis correction factor (default 0.4 -)
  - `K_X` half-saturation coefficient for hydrolysis of XS (default 0.03 -)
  ammonification
  - `κ_a` ammonification rate constant (default 0.08 m3/(gCOD*day))

# States:
  - `S_I`   soluble inert organic matter
  - `S_S`   readily biodegradable substrate
  - `S_O`   oxygen
  - `S_NO`  nitrate and nitrite nitrogen
  - `S_NH`  ammonium and ammonia nitrogen
  - `S_ND`  soluble biodegradable organic nitrogen
  - `S_ALK` alkalinity
  - `X_I`   particulate inert organic matter
  - `X_S`   slowly biodegradable substrate
  - `X_BH`  active heterotrophic biomass
  - `X_BA`  active autotrophic biomass
  - `X_P`   particulate products from biomass decay
  - `X_ND`  particulate biodegradable organic
"""
@component function ASM1(; name, Y_H=0.67, Y_A=0.24, i_XB=0.086, i_XP=0.01,
                         f_P=0.08, μ_H=6.0, K_S=20.0, K_OH=0.2, K_NO=0.5, b_H=0.62, η_g=0.8, μ_A=0.8, K_NH=1.0, K_OA=0.4, b_A=0.1, κ_h=3.0, η_h=0.4, K_X=0.03, κ_a=0.08)

    @variables t
    @named state = StateInputPort(Dict([
           "S_I" => false,  # soluble inert organic matter
           "S_S" => false,  # readily biodegradable substrate
           "S_O" => false,  # oxygen
           "S_NO" => false, # nitrate and nitrite nitrogen
           "S_NH" => false, # ammonium and ammonia nitrogen
           "S_ND" => false, # soluble biodegradable organic nitrogen
           "S_ALK" => false,# alkalinity
           "X_I" => true,   # particulate inert organic matter
           "X_S" => true,   # slowly biodegradable substrate
           "X_BH" => true,  # active heterotrophic biomass
           "X_BA" => true,  # active autotrophic biomass
           "X_P" => true,   # particulate products from biomass decay
           "X_ND" => true   # particulate biodegradable organic nitrogen
           ]))
    @named rates = ReactionOutputPort(states(state))

    # parameters
    stoich_params = @parameters Y_H=Y_H Y_A=Y_A i_XB=i_XB i_XP=i_XP f_P=f_P
    # kinetic parameter heterotrophs
    heterotrophs_params = @parameters μ_H=μ_H K_S=K_S K_OH=K_OH K_NO=K_NO b_H=b_H η_g=η_g
    # kinetic parameter autotrophs
    autotrops_params = @parameters μ_A=μ_A K_NH=K_NH K_OA=K_OA b_A=b_A
    # hydrolysis parameter
    hydrolysis_params = @parameters κ_h=κ_h η_h=η_h K_X=K_X
    # ammonification
    ammonifiaction_params = @parameters κ_a=κ_a

    # Switching function
    switch_func(x, y) = x ./ (x .+ y)

    # Process rates
    aer_gr_het(S_S, S_O, X_BH) = μ_H * switch_func(S_S, K_S) * switch_func(S_O, K_OH) * X_BH
    an_gr_het(S_S, S_O, S_NO, X_BH) = μ_H * switch_func(S_S, K_S) * switch_func(K_OH, S_O) * switch_func(S_NO, K_NO) * η_g * X_BH
    aer_gr_aut(S_NH, S_O, X_BA) = μ_A * switch_func(S_NH, K_NH) * switch_func(S_O, K_OA) * X_BA
    dec_het(X_BH) = b_H * X_BH
    dec_aut(X_BA) = b_A * X_BA
    ammon_solorg_N(S_ND, X_BH) = κ_a * S_ND * X_BH
    hydrol_org(X_BH, X_S, S_O, S_NO) = (
                                κ_h * switch_func(X_S, (X_BH * K_X))
                                * ( switch_func(S_O, K_OH)
                                + η_h * switch_func(K_OH, S_O) * switch_func(S_NO, K_NO) ) * X_BH
                               )
    hydrol_org_N(X_BH, S_O, S_NO, X_ND, X_S) = hydrol_org(X_BH, X_S, S_O, S_NO) * X_ND / X_S


    eqs = [

        rates.S_I ~ 0
        rates.S_S ~ (hydrol_org(state.X_BH, state.X_S, state.S_O, state.S_NO)
                              - (aer_gr_het(state.S_S, state.S_O, state.X_BH) + an_gr_het(state.S_S, state.S_O, state.S_NO, state.X_BH)) / Y_H
                              )
        rates.X_I ~ 0
        rates.X_S ~ ((1 - f_P) * dec_het(state.X_BH) + (1 - f_P) * dec_aut(state.X_BA)
                      - hydrol_org(state.X_BH, state.X_S, state.S_O, state.S_NO)
                    )
        rates.X_BH ~ aer_gr_het(state.S_S, state.S_O, state.X_BH) + an_gr_het(state.S_S, state.S_O, state.S_NO, state.X_BH) - dec_het(state.X_BH)
        rates.X_BA ~ aer_gr_aut(state.S_NH, state.S_O, state.X_BA) - dec_aut(state.X_BA)
        rates.X_P ~ f_P * (dec_het(state.X_BH) + dec_aut(state.X_BA))
        rates.S_O ~ -(1 - Y_H) / Y_H * aer_gr_het(state.S_S, state.S_O, state.X_BH) - (4.57 - Y_A) / Y_A * aer_gr_aut(state.S_NH, state.S_O, state.X_BA)
        rates.S_NO ~ -(1 - Y_H) / (2.86 * Y_H) * an_gr_het(state.S_S, state.S_O, state.S_NO, state.X_BH) + 1 / Y_A * aer_gr_aut(state.S_NH, state.S_O, state.X_BA)
        rates.S_NH ~ (
                      ammon_solorg_N(state.S_ND, state.X_BH)
                      - i_XB * (aer_gr_het(state.S_S, state.S_O, state.X_BH) + an_gr_het(state.S_S, state.S_O, state.S_NO, state.X_BH))
                      - (i_XB + 1/Y_A) * aer_gr_aut(state.S_NH, state.S_O, state.X_BA)
                      )
        rates.S_ND ~ hydrol_org_N(state.X_BH, state.S_O, state.S_NO, state.X_ND, state.X_S) - ammon_solorg_N(state.S_ND, state.X_BH)
        rates.X_ND ~ (
                      (i_XB - f_P * i_XP) * (dec_het(state.X_BH) + dec_aut(state.X_BA)) - hydrol_org_N(state.X_BH, state.S_O, state.S_NO, state.X_ND, state.X_S)
                      )
        rates.S_ALK ~ (
                       ((1 - Y_H) / (14 * 2.86 * Y_H) - i_XB / 14) * an_gr_het(state.S_S, state.S_O, state.S_NO, state.X_BH)
                       - i_XB/14 * aer_gr_het(state.S_S, state.S_O, state.X_BH)
                       - (i_XB / 14 + 1 / (7 * Y_A)) * aer_gr_aut(state.S_NH, state.S_O, state.X_BA)
                       + 1/14 * ammon_solorg_N(state.S_ND, state.X_BH)
                      )
    ]

    parameters = vcat(stoich_params, heterotrophs_params, autotrops_params, hydrolysis_params, ammonifiaction_params)

    compose(ODESystem(eqs, t, [], parameters; name=name), [state, rates])
  end

