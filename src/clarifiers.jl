"""
    IdealClarifier(states; f_ns = 0, name)

An Ideal Clarifier.
This system defines an ideal clarifier. It is a static system that ensures mass conservation and ideally 
settles all particulate compounds: a parameter for the unsettlable fraction of solids can be given.
The flow rates of the outflows (effluent or sludge) are to be provided by the following systems, e.g. using a pump system.

# Parameters:
    - `states`: The components that the clarifier should have
    - `f_ns`: The unsettlable fraction of solids

# Connectors:
    - `inflow`: The inflow of the IdealClarifier
    - `effluent`: The effluent of the IdealClarifier
    - `sludge`: The the sludge outflow of the IdealClarifier
"""
@component function IdealClarifier(states; f_ns = 0, name)
    # Get all connectors
    @named inflow = InflowPort(states)
    @named effluent = OutflowPort(states)
    @named sludge = OutflowPort(states)
    
    # The parameter for the fraction of non-settlable solids
    ps = @parameters f_ns = f_ns

    # Time as the Independent Variable
    @variables t

    # Mass conservation: flowrates (inflow rate = outflow rate)
    eqs = [inflow.q ~ effluent.q + sludge.q]
    # Mass conservation: Component masses q_in*c_in = q_eff*c_eff + q_s*c_s
    for statename in symbolic_to_namestring.(ModelingToolkit.states(inflow))
        if statename != "q" # Not for q itself
            push!(eqs, getproperty(inflow, Symbol(statename)) * inflow.q ~ getproperty(effluent, Symbol(statename)) * effluent.q + getproperty(sludge, Symbol(statename)) * sludge.q)
        end
    end
    # Solids separation: If solid, only f_ns of inflow in effluent, otherwise equal concentration as in inflow (sludge follows from mass conservation)
    for state in ModelingToolkit.states(inflow)
        statename = symbolic_to_namestring(state)
        if statename != "q" # Not for q itself
            if isparticulate(state) # Particulate component
                push!(eqs, getproperty(effluent, Symbol(statename)) ~ f_ns * getproperty(inflow, Symbol(statename))) 
            else # Soluble component
                push!(eqs, getproperty(effluent, Symbol(statename)) ~ getproperty(inflow, Symbol(statename)))
            end
        end
    end

    # Compose and return the IdealClarifier system
    compose(ODESystem(eqs, t, [], ps; name=name), [inflow, effluent, sludge])
end