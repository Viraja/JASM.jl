"""
    FlowConnector(state; n_in, n_out, name)

A component for connecting multiple flows.

This system takes an arbitrary number of inputs and splits it into a number of outputs. 
Thereby, it is ensuring mass conservation and assumes ideal mixing between the incoming flows
such that the concentrations in all outflows are equal. The flow rates of the outflows are to be 
provided by the following systems, e.g. using a pump system.

Specifically, it has the following equations (``C`` always stands for an arbitrary component):
- Conservation of mass:
```math
\\begin{aligned}
\\sum_{i = 0}^{n_{in}}q_{in,i} &= \\sum_{i=0}^{n_{out}}q_{out,i}\\\\
\\sum_{i = 0}^{n_{in}}C_{in,i}q_{in,i} &= \\sum_{i=0}^{n_{out}}C_{out,i}q_{out,i}
\\end{aligned}
```
- Ideal mixing:
```math
C_{out,i} = C_{out,j} \\quad \\forall i,j\\in \\{1 ... n_{out}\\}
```

# Parameters:
- `states`: The components in the flows
- `n_in`: The number of inflows
- `n_out`: The number of outflows

# Connectors:
- `inflow_i`: The i-th inflow of the `FlowConnector` (if only one inflow, the `_i` is omitted and the resulting name is `inflow`)
- `outflow_i`: The i-th outflow of the `FlowConnector` (if only one inflow, the `_i` is omitted and the resulting name is `outflow`)
"""
@component function FlowConnector(state; n_in, n_out, name)
    # Get all inflow and outflow connectors
    inflows = ODESystem[]
    if n_in == 1
        push!(inflows, InflowPort(state; name = :inflow))
    else
        for i = 1:n_in
            push!(inflows, InflowPort(state; name = Symbol("inflow_"*string(i))))
        end
    end
    outflows = ODESystem[]
    if n_out == 1
        push!(outflows, OutflowPort(state; name = :outflow))
    else
        for i = 1:n_out
            push!(outflows, OutflowPort(state; name = Symbol("outflow_"*string(i))))
        end
    end

    # Time as the Independent Variable
    @variables t

    # Assemble the equations
    eqs = Equation[]
    # Mass conservation: Flows (inflow = outflow)
    push!(eqs, sum(getproperty.(inflows, :q)) ~ sum(getproperty.(outflows, :q)))
    # Mass conservation: Component masses sum(q_in*c_in) = sum(q_out*c_out)
    for statename in symbolic_to_namestring.(states(outflows[1]))
        if statename != "q"
            push!(eqs, sum(getproperty.(inflows, Symbol(statename)) .* getproperty.(inflows, :q)) ~ sum(getproperty.(outflows, Symbol(statename)) .* getproperty.(outflows, :q)))
        end
    end
    # All components equal in all outflows
    for outflow = outflows[2:end]
        # Equate all components
        for statename in symbolic_to_namestring.(states(outflows[1]))
            if statename != "q"
                push!(eqs, Equation(getproperty.([outflows[1], outflow], Symbol(statename))...))
            end
        end
    end

    # Compose and return the ODESystem
    compose(ODESystem(eqs, t, [], []; name=name), [inflows..., outflows...])
end

"""
    FlowUnifier(state; n_in, name)

A component for unifying multiple flows into a single one.
It is an alias for the `FlowConnector` having only one outflow.

# Parameters:
- `states`: The components in the flows
- `n_in`: The number of inflows

# Connectors:
- `inflow_i`: The i-th inflow of the `FlowUnifier`
- `outflow`: The outflow of the `FlowUnifier`
"""
FlowUnifier(state; n_in, name) = FlowConnector(state; n_in=n_in, n_out=1, name)

"""
    FlowSeparator(state; n_out, name)

A component for separating a single flow into multiple.
It is an alias for the `FlowConnector` having only one inflow.

# Parameters:
- `states`: The components in the flows
- `n_out`: The number of outflows

# Connectors:
- `inflow`: The inflow of the `FlowSeparator`
- `outflow_i`: The i-th outflow of the `FlowSeparator`
"""
FlowSeparator(state; n_out, name) = FlowConnector(state; n_in=1, n_out=n_out, name)

"""
  FlowPump(state; flowrate=nothing, name)

A component fmodeling a flow pump.
It models an ideal flow pump, where a provided flow is pumped through.

# Parameters:
- `states`: The components in the flows
- `flowrate`: The flowrate to pump. If nothing an input is added for setting it dynamically

# Connectors:
- `inflow`: The inflow of the Pump
- `outflow`: The outflow of the Pump
- `q_pumped`: **Optional**: [`ModelingToolkitStandardLibrary.Blocks.RealInput`](@ref) to provide the pumped flow rate. Added only if the parameter `flowrate` is `nothing`, otherwise the flowrate is considered fixed at the given value.
"""
@component function FlowPump(state; flowrate=nothing, name)
    # Time as the Independent Variable
    @variables t

    # Get inflow and outflow ports
    @named inflow = InflowPort(state)
    @named outflow = OutflowPort(state)

    # Equations: Connecting inflow and outflow (must be equal)
    eqs = [
        connect(inflow, outflow)
    ]

    # Enforce pumped flow (either from input port or fixed if given) and create system
    if isnothing(flowrate)
        @named q_pumped = ModelingToolkitStandardLibrary.Blocks.RealInput(nin=1)
        push!(eqs, inflow.q ~ q_pumped.u)
        compose(ODESystem(eqs, t, [], []; name), [inflow, outflow, q_pumped])
    else
        @parameters q_pumped = flowrate
        push!(eqs, inflow.q ~ q_pumped)
        compose(ODESystem(eqs, t, [], [q_pumped]; name), [inflow, outflow])
    end
end