"""
    Aeration(; name, k_La, S_O_max)

A component that defines a simple the aeration process using the oxygen transfer coefficient k_L*a and the oxygen saturation concentration.

# Parameters:
  - `k_La` initial oxygen transfer coefficient (default 240 1/d)
  - `S_O_max` the saturation concentration for oxygen (default 8 g/m^3) 

# States:
  - `S_O` oxygen

# Connectors
  - `state` The reactor state to connect (only dissolved oxygen)
  - `rates` The calculated rates (dissolved oxygen only)
  - `k_La` The oxygen transfer coefficent 
"""
@component function Aeration(; name, k_La = 240, S_O_max = 8)

    @variables t
    @named state = StateInputPort(Dict([
           "S_O" => false,  # oxygen
           ]))
    @named rates = ReactionOutputPort(states(state))

    # variables
    @named k_La = ModelingToolkitStandardLibrary.Blocks.RealInput(nin=1, u_start=k_La)
    # parameters
    parameters = @parameters S_O_max=S_O_max

    eqs = [
        rates.S_O ~ k_La.u * (S_O_max - state.S_O)
    ]

    compose(ODESystem(eqs, t, [], parameters; name=name), [state, rates, k_La])
  end