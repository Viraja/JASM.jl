# Copyright (C) 2023 Juan Pablo Carbajal
# Copyright (C) 2023 Florian Wenk
# Copyright (C) 2023 Mariane Yvonne Schneider
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Author: Florian Wenk <florian.wenk@eawag.ch>
# Author: Mariane Yvonne Schneider <myschneider@meiru.ch>
# Date: 08.08.2023


"""
    OzoneDisinfection(; name, kO3, kd)

A component that defines two processes in a ozone disinfection i.e. decay of ozone and disinfection. This can be used as an example for testing the package.

# Parameters:
    - `kO3` the ozone decay constant
    - `kD` the disinfection constant

# States:
    - `S_O3` the dissolved ozone concentration
    - `X_B` the bacteria concentration (particulate)
"""
@component function OzoneDisinfection(; name, kO3=10, kd=1500)
    @named state = StateInputPort(Dict([
           "S_O3" => false, # soluble ozone concentration
           "X_B" => true,   # bacteria concentration
           ]))
    @named rates = ReactionOutputPort(states(state))

    # parameters
    parameters = @parameters kO3=kO3 kd=kd

    eqs = [
        rates.S_O3 ~ -kO3 * state.S_O3             # decay of ozone
        rates.X_B ~ -kd * state.X_B * state.S_O3   # disinfection
    ]

    compose(ODESystem(eqs, t, [], parameters; name=name), [state, rates])
  end

