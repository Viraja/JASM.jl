function growth_ode(x; μ, δ, name)
    @parameters t
    D = Differential(t)

    ODESystem(D(x) ~ (μ - δ) * x, t; name = name)
end

"""
    monod_moser(x, y; μmax, K [, decay=nothing, n=1], name)

Population dynamics model with Monod (n=1) or Moser (n ≠ 1) growth rate.

# Examples
```@example
using ModelingToolkit
@variables t S(t) B(t)
@parameters u k
@named monod = monod_moser(S, B; μmax = u, K = k)
```
"""
function monod_moser(x, y; μmax, K, decay=nothing, n=1, name)
    u = μmax * y^n / (K + y^n)
    d = isnothing(decay) ? μmax / 20 : decay
    growth_ode(x; μ = u, δ = d, name = name)
end
