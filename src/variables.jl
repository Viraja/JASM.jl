# Variable Annotations

# If the component is particulate
struct VariableParticulate <: Symbolics.AbstractVariableMetadata end

Symbolics.option_to_metadata_type(::Val{:particulate}) = VariableParticulate

isparticulate(x::Symbolics.Num; default = nothing) = isparticulate(Symbolics.unwrap(x); default = default)

"""
    isparticulate(x; default = nothing)

Determine if the symbolic variable `x` is marked as particulate or not.

`default` indicates how unlabeled variables should be treated, by default it is `nothing` to be able to distinct between labeled and unlabeled variables.
"""
function isparticulate(x; default = nothing)
    p = Symbolics.getparent(x, nothing)
    p === nothing || (x = p)
    Symbolics.getmetadata(x, VariableParticulate, default)
end

