"""
    CSTR(V, states; initial_states = nothing, name)

A Continuously Stirred Tank Reactor (CSTR).
This system defines the mass flow of a CSTR without any reactions. The reactions are to be supplied from an external system.
The mass transfer of an given component ``C`` in a CSTR with volume ``V`` and flow rate ``q`` is defined as:
    ``\\frac{dC}{dt} = (C_{in} - C) * \\frac{q}{V} + r_C``
where ``r_C`` is the externally supplied reaction rate corresponding to the component.

# Parameters:
    - `V`: The volume of the CSTR
    - `states`: The components that the CSTR should have and for which the mass transfer is defined
    - `initial_states`: The initial concentrations in the CSTR

# States:
    - One state for each component defined by the `states` parameter

# Connectors:
    - `inflow`: The inflow of the CSTR
    - `outflow`: The outflow of the CSTR
    - `state`: The current values of all states in the CSTR
    - `rates`: Input for the reaction rates for each state
"""
@component function CSTR(V, states; initial_states = nothing, name)
    # Get all connectors
    @named inflow = InflowPort(states)
    @named rates = ReactionInputPort(states)
    state = isnothing(initial_states) ? StateOutputPort(states; name = :state) : StateOutputPort(states; initial_states, name = :state)
    @named outflow = OutflowPort(states)
    # The parameter for the Volume
    ps = @parameters V = V

    # Time as the Independent Variable and its derivative
    @variables t
    D = Differential(t)

    # The CSTR equation (inflow rate = outflow rate)
    eqs = [inflow.q ~ outflow.q]
    # For each Component c add the following equations:
    #   dc/dt = c_in * q_in/V - c * q_out/V + r_c (Conservation of mass + reaction term (supplied from connector))
    #   c_out = c (The output is equal to the internal state)
    for sym = Symbol.(symbolic_to_namestring.(ModelingToolkit.states(state)))
        in = getproperty(inflow, sym)
        r = getproperty(rates, sym)
        s = getproperty(state, sym)
        out = getproperty(outflow, sym)
        push!(eqs, D(s) ~ in * inflow.q / V - s * outflow.q / V + r)
        push!(eqs, out ~ s)
    end

    # Compose and return the CSTR system
    compose(ODESystem(eqs, t, ModelingToolkit.states(state), ps; name=name), [inflow, rates, state, outflow])
end
